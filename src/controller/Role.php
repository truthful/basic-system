<?php

declare(strict_types=1);

namespace app\sys\controller;

use app\sys\service\SystemMenuService;
use think\admin\Controller;
use think\admin\model\SysRelation;
use think\admin\model\SysRole;
use think\admin\model\SysUser;
use think\db\exception\DataNotFoundException;
use think\db\exception\DbException;
use think\db\exception\ModelNotFoundException;

/**
 * 角色接口
 * Class Role
 * @package app\sys\controller
 */
class Role extends Controller
{
    /**
     * 角色分页列表
     * @auth true
     * @return void
     * @throws DataNotFoundException
     * @throws DbException
     * @throws ModelNotFoundException
     */
    public function page()
    {
        $query = SysRole::mQuery();
        $query->where(['is_deleted' => 0]);

        // 数据列表搜索过滤
        $query->equal('status')->dateBetween('create_time');
        $query->like('name,category');
        $query->dataScope('created_by');
        $lists = $query->order('sort ASC,id ASC')->page();
    }

    /**
     * 获取一条角色详情
     * @auth true
     * @return void
     */
    public function detail()
    {
        sysoplog('角色管理', '角色详情获取成功');
        $this->success('操作成功', SysRole::detail($this->request->param('id')));
    }

    /**
     * 添加角色
     * @auth true
     * @return void
     */
    public function add()
    {
        sysoplog('角色管理', '角色添加成功');
        SysRole::mForm('form');
    }

    /**
     * 更新角色
     * @auth true
     * @return void
     */
    public function edit()
    {
        sysoplog('角色管理', '角色更新成功');
        SysRole::mForm('form');
    }

    /**
     * 修改角色状态
     * @auth true
     * @return void
     */
    public function state()
    {
        sysoplog('角色管理', '角色修改成功');
        SysRole::mSave($this->_vali([
            'status.in:0,1' => '状态值范围异常！',
            'status.require' => '状态值不能为空！',
        ]));
    }

    /**
     * 查询角色已授权用户
     * @auth true
     * @return void
     */
    public function ownUser()
    {
        $users = SysRelation::getTargetIds('SYS_ROLE_HAS_USER', $this->request->param('id'));
        sysoplog('角色管理', '角色授权用户成功');
        $this->success('获取成功！', $users);
    }

    /**
     * 资源选择器
     * @auth true
     * @return void
     * @throws DataNotFoundException
     * @throws DbException
     * @throws ModelNotFoundException
     */
    public function resourceTreeSelector()
    {
        $menus = SystemMenuService::instance()->getResourceTree();
        $this->success('获取成功！', $menus);
    }

    /**
     * 权限选择器
     * @auth true
     * @return void
     * @throws DataNotFoundException
     * @throws DbException
     * @throws ModelNotFoundException
     */
    public function permissionTreeSelector()
    {
        $permissions = SystemMenuService::instance()->getPermissions($this->request->param('category', 'biz'));
        $this->success('获取成功！', $permissions);
    }

    /**
     * 移动端资源选择器
     * @auth true
     * @return void
     * @throws DataNotFoundException
     * @throws DbException
     * @throws ModelNotFoundException
     */
    /*public function mobileMenuTreeSelector()
    {
        $menus = SystemMenuService::instance()->getResourceTree();
        $this->success('获取成功！',$menus);
    }*/

    /**
     * 查询角色已授权的资源
     * @auth true
     * @return void
     */
    public function ownResource()
    {
        $data['id'] = $this->request->param('id');
        $grantInfoList = SysRelation::mk()
            ->whereIn('object_id', $this->request->param('id'))
            ->where(['category' => 'SYS_ROLE_HAS_RESOURCE'])
            ->column('ext_json');
        $data['grantInfoList'] = [];
        if ($grantInfoList) {
            foreach ($grantInfoList as $v) {
                $data['grantInfoList'][] = json_decode($v, true);
            }
        }
        $this->success('获取成功！', $data);
    }

    /**
     * 查询角色已授权的权限
     * @auth true
     * @return void
     */
    public function ownPermission()
    {
        $data['id'] = $this->request->param('id');
        $grantInfoList = SysRelation::mk()
            ->whereIn('object_id', $this->request->param('id'))
            ->where(['category' => 'SYS_ROLE_HAS_PERMISSION'])
            ->column('ext_json');
        $data['grantInfoList'] = [];
        if ($grantInfoList) {
            foreach ($grantInfoList as $v) {
                $data['grantInfoList'][] = json_decode($v, true);
            }
        }
        $this->success('获取成功！', $data);
    }

    /**
     * 移动端角色已授权资源
     * @auth true
     * @return void
     */
    /*public function ownMobileMenu()
    {
        $data['id'] = $this->request->param('id');
        $data['grantInfoList'] = SysUserRole::mk()->where(['user_id'=>$data['id']]);
        $this->success('获取成功！',$data);
    }*/


    /**
     * 主管选择器
     * @auth true
     * @return void
     * @throws DataNotFoundException
     * @throws DbException
     * @throws ModelNotFoundException
     */
    public function userSelector()
    {
        $query = SysUser::mQuery();
        $query->where(['is_deleted' => 0, 'status' => 0]);

        // 数据列表搜索过滤
        $query->like('category');
        $query->field('id,name,account');
        $users = $query->order('id ASC')->page(false,false,false);
        sysoplog('账号管理', '主管选择器获取成功');
        $this->success('数据获取成功', $users['list']);
    }

    /**
     * 角色资源授权
     * @auth true
     * @return void
     * @throws DbException
     */
    public function grantResource()
    {
        $id = $this->request->post('id');
        $grantInfoList = $this->request->post('grantInfoList');
        if ($id) { //角色ID存在
            if (!empty($grantInfoList)) {//菜单、按钮数组存在
                SysRelation::mk()->where(['object_id' => $id, 'category' => 'SYS_ROLE_HAS_RESOURCE'])->delete();
                $data = [];
                foreach ($grantInfoList as $v) {
                    if (!isset($v['buttonInfo'])) {
                        $v['buttonInfo'] = [];
                    }
                    $data[] = [
                        'object_id' => $id,
                        'target_id' => $v['menuId'],
                        'category' => 'SYS_ROLE_HAS_RESOURCE',
                        'ext_json' => json_encode($v)
                    ];
                }
                if (SysRelation::mk()->insertAll($data)) {
                    $this->success('资源授权成功');
                }
            } else {//角色ID存在，菜单、按钮数组ID为空则删除该角色所有菜单
                if (SysRelation::mk()->where(['object_id' => $id, 'category' => 'SYS_ROLE_HAS_RESOURCE'])->delete()) {
                    $this->success('资源授权已删除');
                }
            }
        } else {//角色ID不存在
            $this->error('角色ID不存在，资源授权失败。');
        }
    }

    /**
     * 移到回收站
     * @auth true
     * @return void
     */
    public function delete()
    {
        sysoplog('角色管理', '角色删除成功');
        SysRole::mSave(['is_deleted' => 1]);
    }

    /**
     * 角色授权用户
     * @auth true
     * @return void
     * @throws DbException
     */
    public function grantUser()
    {
        $id = $this->request->post('id');
        $grantInfoList = $this->request->post('grantInfoList');
        if ($id) {//角色ID存在
            if (!empty($grantInfoList)) {//用户列表存在
                SysRelation::mk()->where(['object_id' => $id, 'category' => 'SYS_ROLE_HAS_USER'])->delete();
                $data = [];
                foreach ($grantInfoList as $v) {
                    $data[] = [
                        'object_id' => $id,
                        'target_id' => $v,
                        'category' => 'SYS_ROLE_HAS_USER'
                    ];
                }
                if (SysRelation::mk()->insertAll($data)) {
                    $this->success('用户授权成功');
                }
            } else {//角色ID存在，用户ID为空则删除该角色所有用户
                if (SysRelation::mk()->where(['object_id' => $id, 'category' => 'SYS_ROLE_HAS_USER'])->delete()) {
                    $this->success('用户授权已删除');
                }
            }
        } else {//角色ID不存在
            $this->error('角色ID不存在，用户授权失败。');
        }
    }

    /**
     * 角色授权权限
     * @auth true
     * @return void
     * @throws DbException
     */
    public function grantPermission()
    {
        $id = $this->request->post('id');
        $grantInfoList = $this->request->post('grantInfoList');
        if ($id) { //角色ID存在
            if (!empty($grantInfoList)) {//权限数组存在
                SysRelation::mk()->where(['object_id' => $id, 'category' => 'SYS_ROLE_HAS_PERMISSION'])->delete();
                $data = [];
                foreach ($grantInfoList as $v) {
                    if (!isset($v['scopeDefineOrgIdList'])) {
                        $v['scopeDefineOrgIdList'] = [];
                    }
                    $data[] = [
                        'object_id' => $id,
                        'target_id' => $v['apiUrl'],
                        'category' => 'SYS_ROLE_HAS_PERMISSION',
                        'ext_json' => json_encode($v)
                    ];
                }
                if (SysRelation::mk()->insertAll($data)) {
                    $this->success('权限授权成功');
                }
            } else {//角色ID存在，权限数组为空则删除该角色所有菜单
                if (SysRelation::mk()->where(['object_id' => $id, 'category' => 'SYS_ROLE_HAS_PERMISSION'])->delete()) {
                    $this->success('权限授权已删除');
                }else{
                    $this->error('权限不存在，请先选择接口');
                }
            }
        } else {//角色ID不存在
            $this->error('角色ID不存在，权限授权失败。');
        }
    }
}