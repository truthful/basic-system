<?php

declare(strict_types=1);

namespace app\sys\controller;

use app\sys\service\syncService;
use app\sys\service\SystemMenuService;
use Exception;
use think\admin\Controller;
use think\admin\model\SysPublicCategory;
use think\db\exception\DataNotFoundException;
use think\db\exception\DbException;
use think\db\exception\ModelNotFoundException;
use think\exception\HttpResponseException;


/**
 * 商品公共分类管理
 * class PublicCategory
 * @package app\sys\controller
 */
class PublicCategory extends Controller
{
    /**
     * 商品公共分类分页列表
     * @auth true
     * @return void
     * @throws DataNotFoundException
     * @throws DbException
     * @throws ModelNotFoundException
     */
    public function page()
    {
        $query = SysPublicCategory::mQuery();
        $query->where(['is_deleted' => 0]);
        // 数据列表搜索过滤
        $query->equal('status,parent_id')->dateBetween('create_time');
        $query->like('name');
        $lists = $query->order('sort desc,id asc')->page(false, false, false);
        if (count($lists['list']) > 0) $lists['list'] = SystemMenuService::instance()->toTree($lists['list']);
        $this->success('数据获取成功', $lists['list']);
    }

    /**
     * 商品公共分类列表
     * @login true
     * @return void
     * @throws DataNotFoundException
     * @throws DbException
     * @throws ModelNotFoundException
     */
    public function list()
    {
        $lists = SysPublicCategory::mk()
            ->where(['is_deleted' => '0', 'status' => 0])
            ->order('id DESC')
            ->select()
            ->toArray();
        $this->success('数据获取成功', $lists['list']);
    }

    /**
     * 获取分类选择树
     * @login true
     * @return void
     * @throws DataNotFoundException
     * @throws DbException
     * @throws ModelNotFoundException
     */
    public function tree()
    {
        $trees = SysPublicCategory::mk()
            ->where(['is_deleted' => 0])
            ->where(['status' => 0])
            ->field('id,parent_id,name,id as value')
            //->field('id,parent_id,name as label')
            ->order('sort desc')
            ->select()
            ->toArray();
        $lists = SystemMenuService::instance()->toTree($trees);
        $this->success('数据获取成功', $lists);
    }

    /**
     * 获取一条公共分类详情
     * @auth true
     * @return void
     */
    public function detail()
    {
        sysoplog('商品公共分类管理', '商品公共分类详情获取成功');
        $this->success('操作成功', SysPublicCategory::detail($this->request->param('id')));
    }

    /**
     * 添加商品公共分类
     * @auth true
     * @return void
     */
    public function add()
    {
        sysoplog('商品公共分类管理', '新增商品公共分类');
        SysPublicCategory::mForm('form');
    }

    /**
     * 编辑商品公共分类
     * @auth true
     * @return void
     */
    public function edit()
    {
        sysoplog('商品公共分类管理', '编辑商品公共分类');
        SysPublicCategory::mForm('form');
    }

    /**
     * 修改状态
     * @auth true
     * @return void
     */
    public function state()
    {
        sysoplog('分类管理', '修改分类状态');
        SysPublicCategory::mSave($this->_vali([
            'status.in:0,1' => '状态值范围异常！',
            'status.require' => '状态值不能为空！',
        ]));
    }

    /**
     * 同步商品公共分类
     * @auth true
     * @return void
     */
    public function sync()
    {
        try {
            $result = syncService::syncData('sync/goods');
            if (empty($result['code'])) $this->error($result['info']);
            set_time_limit(100);
            foreach ($result['data'] as $vo) SysPublicCategory::mUpdate(['goods_cate_id' => $vo['goods_cate_id'], 'name' => $vo['name'], 'parent_id' => $vo['parent_id'], 'level' => $vo['level'], 'is_deleted' => 0], 'goods_cate_id');
            $this->success('同步商品公共分类成功！');
        } catch (HttpResponseException $exception) {
            throw $exception;
        } catch (Exception $exception) {
            $this->error('同步商品公共分类数据失败！');
        }
    }

    /**
     * 删除商品公共分类
     * @auth true
     * @return void
     * @throws DataNotFoundException
     * @throws DbException
     * @throws ModelNotFoundException
     */
    public function delete()
    {
        $id = $this->request->param('id');//input('id');
        $ids = explode(',', $id);

        if ($this->checkChildrenExists($ids)) {
            $this->error('您要删除的分类下还有下级分类，如需删除请先删除下级分类后再删除该分类。');
        } else {
            sysoplog('分类管理', '删除分类');
            SysPublicCategory::mSave(['is_deleted' => 1, 'status' => 1], 'id', [['id', 'in', $ids]]);
        }
    }

    /**
     * 检查子分类是否存在
     * @param array $ids
     * @return bool
     * @throws DataNotFoundException
     * @throws DbException
     * @throws ModelNotFoundException
     */
    public function checkChildrenExists(array $ids): bool
    {
        if (SysPublicCategory::mk()->whereIn('parent_id', $ids)->where(['is_deleted' => 0])->find()) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * 表单数据处理
     * @param array $data
     * @return void
     */
    protected function _form_filter(array &$data)
    {
        if (!empty($data['id']) && $data['id'] === $data['parent_id']) {
            $this->error('上级分类不能为本分类');
        }
    }

}
