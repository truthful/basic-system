<?php

declare(strict_types=1);

namespace app\sys\controller;

use app\sys\service\SystemMenuService;
use think\admin\Controller;
use think\admin\model\SysFileGroup;
use think\db\exception\DataNotFoundException;
use think\db\exception\DbException;
use think\db\exception\ModelNotFoundException;


/**
 * 文件分组接口
 * Class Group
 * @package app\sys\controller
 */
class Group extends Controller
{
    /**
     * 文件分组分页列表
     * @auth true
     * @return void
     * @throws DataNotFoundException
     * @throws DbException
     * @throws ModelNotFoundException
     */
    public function page()
    {
        $query = SysFileGroup::mQuery();
        $query->where(['is_deleted' => 0]);

        // 数据列表搜索过滤
        $query->equal('status')->dateBetween('create_time');
        $query->like('name,code');
        $lists = $query->order('id ASC')->page();
    }


    /**
     * 文件分组树
     * @auth true
     * @return void
     * @throws DataNotFoundException
     * @throws DbException
     * @throws ModelNotFoundException
     */
    public function tree()
    {
        $trees = SysFileGroup::mk()
            ->where(['is_deleted' => 0])
            ->field('id,parent_id,name as title,id as value')
            //->field('id,parent_id,name as label')
            ->order('id asc')
            ->select()
            ->toArray();
        $lists = SystemMenuService::instance()->toTree($trees);
        $this->success('数据获取成功', $lists['list']);
    }

    /**
     * 文件分组列表
     * @auth true
     * @return void
     * @throws DataNotFoundException
     * @throws DbException
     * @throws ModelNotFoundException
     */
    public function list()
    {
        $lists = SysFileGroup::mk()
            ->where(['is_deleted' => 0])
            ->field('id,parent_id,name as title,id as value')
            //->field('id,parent_id,name as label')
            ->order('id asc')
            ->select()
            ->toArray();
        $this->success('数据获取成功', $lists['list']);
    }


    /**
     * 文件分组保存
     * @auth true
     * @return void
     */
    public function save()
    {
        SysFileGroup::mForm('form');
    }

    /**
     * 更新文件分组
     * @auth true
     * @return void
     */
    public function update()
    {
        SysFileGroup::mForm('form');
    }


    /**
     * 移到回收站
     * @auth true
     * @return void
     */
    public function remove()
    {
        $id = $this->request->param('id');//intval(input('id'));
        if ($this->checkChildrenExists($id)) {
            $this->error('您要删除的菜单下还有下级菜单，如需删除请先删除下级菜单后再删除该菜单。');
        } else {
            SysFileGroup::mSave(['is_deleted' => 1]);
        }
    }

    /**
     * 检查子分组是否存在
     * @param int $id
     * @return bool
     * @throws DataNotFoundException
     * @throws DbException
     * @throws ModelNotFoundException
     */
    public function checkChildrenExists(int $id): bool
    {
        if (SysFileGroup::mk()->where('parent_id', $id)->where(['is_deleted' => 0])->find()) {
            return true;
        } else {
            return false;
        }
    }

}