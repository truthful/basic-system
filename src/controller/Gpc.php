<?php

declare(strict_types=1);

namespace app\sys\controller;

use app\sys\service\syncService;
use app\sys\service\SystemMenuService;
use Exception;
use think\admin\Controller;
use think\admin\model\SysGpc;
use think\db\exception\DataNotFoundException;
use think\db\exception\DbException;
use think\db\exception\ModelNotFoundException;
use think\exception\HttpResponseException;

/**
 * 国际GPC分类接口
 * Class Gpc
 * @package app\sys\controller
 */
class Gpc extends Controller
{
    /**
     * GPC分页列表
     * @auth true
     * @return void
     * @throws DataNotFoundException
     * @throws DbException
     * @throws ModelNotFoundException
     */
    public function page()
    {
        $query = SysGpc::mQuery();
        $query->where(['is_deleted' => 0]);
        $query->where(['parent_id' => intval($this->request->param('parentId', 0))]);
        // 数据列表搜索过滤
        $query->equal('status')->dateBetween('create_time');
        $query->like('description,description_en,code');
        $lists = $query->order('sort ASC,id ASC')->page();
    }

    /**
     * 获取GPC选择树
     * @login true
     * @return void
     * @throws DataNotFoundException
     * @throws DbException
     * @throws ModelNotFoundException
     */
    public function tree()
    {
        $gpc = SysGpc::mk()
            ->where(['is_deleted' => 0, 'status' => 0])
            ->order('id asc')
            ->field('*,parent_id as parentId')
            ->select()
            ->toArray();
        $lists = SystemMenuService::instance()->toTree($gpc);
        sysoplog('GPC管理', 'GPC树获取成功');
        $this->success('数据获取成功', $lists);
    }

    /**
     * 获取一条GPC详情
     * @auth true
     * @return void
     */
    public function detail()
    {
        sysoplog('GPC管理', 'GPC获取成功');
        $this->success('操作成功', SysGpc::detail($this->request->param('id')));
    }

    /**
     * 添加GPC
     * @auth true
     * @return void
     */
    public function add()
    {
        sysoplog('GPC管理', 'GPC新增成功');
        SysGpc::mForm('form');
    }

    /**
     * 更新GPC
     * @auth true
     * @return void
     */
    public function edit()
    {
        sysoplog('GPC管理', 'GPC更新成功');
        SysGpc::mForm('form');
    }

    /**
     * 同步GPC分类
     * @auth true
     * @return void
     */
    public function sync()
    {
        try {
            set_time_limit(180);
            $result = syncService::syncAllData('sync/gpc',100);
            $batchSize = 500; // 你可以根据你的数据库性能调整这个值
            $chunks = array_chunk($result, $batchSize);
            foreach ($chunks as $chunk) {
                SysGpc::mk()->insertAll($chunk);
            }
            $this->success('同步GPC分类成功！');
        } catch (HttpResponseException $exception) {
            throw $exception;
        } catch (Exception $exception) {
            $this->error('同步GPC分类数据失败！');
        }
    }

    /**
     * 修改GPC状态
     * @auth true
     * @return void
     */
    public function state()
    {
        sysoplog('GPC管理', 'GPC状态更改成功');
        SysGpc::mSave($this->_vali([
            'status.in:0,1' => '状态值范围异常！',
            'status.require' => '状态值不能为空！',
        ]));
    }

    /**
     * 移到回收站
     * @auth true
     * @return void
     */
    public function delete()
    {
        sysoplog('GPC管理', 'GPC删除成功');
        SysGpc::mSave(['is_deleted' => 1]);
    }

    /**
     * GPC树选择器
     * @login true
     * @return void
     * @throws DataNotFoundException
     * @throws DbException
     * @throws ModelNotFoundException
     */
    public function gpcTreeSelector()
    {
        $gpcs = SysGpc::mk()
            ->where(['is_deleted' => 0, 'status' => 0])
            ->order('id asc')
            ->field('*,parent_id as parentId')
            ->select()
            ->toArray();
        $lists = SystemMenuService::instance()->toTree($gpcs);
        sysoplog('GPC管理', 'GPC树选择器获取成功');
        $this->success('数据获取成功', $lists['list']);
    }
}