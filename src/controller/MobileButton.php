<?php

declare(strict_types=1);

namespace app\sys\controller;

use think\admin\Controller;
use think\admin\model\SysMobileMenu;
use think\db\exception\DataNotFoundException;
use think\db\exception\DbException;
use think\db\exception\ModelNotFoundException;

/**
 * 移动菜单按钮接口
 * Class Button
 * @package app\sys\controller
 */
class MobileButton extends Controller
{
    /**
     * 移动菜单按钮分页列表
     * @auth true
     * @return void
     * @throws DataNotFoundException
     * @throws DbException
     * @throws ModelNotFoundException
     */
    public function page()
    {
        $query = SysMobileMenu::mQuery();
        $query->where(['is_deleted' => 0]);

        // 数据列表搜索过滤
        $query->equal('parent_id,status')->dateBetween('create_time');
        $query->like('name,code,category');
        $lists = $query->order('sort ASC,id ASC')->page();
    }

    /**
     * 获取一条移动菜单按钮详情
     * @auth true
     * @return void
     */
    public function detail()
    {
        sysoplog('移动菜单按钮', '移动菜单按钮获取成功');
        $this->success('操作成功', SysMobileMenu::detail($this->request->param('id')));
    }

    /**
     * 添加移动菜单按钮
     * @auth true
     * @return void
     */
    public function add()
    {
        sysoplog('移动菜单按钮', '移动菜单按钮添加成功');
        SysMobileMenu::mForm('form');
    }

    /**
     * 更新移动菜单按钮
     * @auth true
     * @return void
     */
    public function edit()
    {
        sysoplog('移动菜单按钮', '移动菜单按钮修改成功');
        SysMobileMenu::mForm('form');
    }

    /**
     * 修改移动菜单按钮状态
     * @auth true
     * @return void
     */
    public function state()
    {
        sysoplog('移动菜单按钮', '移动菜单按钮状态更新成功');
        SysMobileMenu::mSave($this->_vali([
            'status.in:0,1' => '状态值范围异常！',
            'status.require' => '状态值不能为空！',
        ]));
    }

    /**
     * 移到回收站
     * @auth true
     * @return void
     */
    public function delete()
    {
        sysoplog('移动菜单按钮', '移动菜单按钮删除成功');
        SysMobileMenu::mSave(['is_deleted' => 1]);
    }
}