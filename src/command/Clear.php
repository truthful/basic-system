<?php

declare (strict_types=1);

namespace app\sys\command;

use think\admin\model\SysWechatPaymentRecord;
use think\admin\Command;
use think\console\Input;
use think\console\Output;

/**
 * 微信支付单清理任务
 * @class Clear
 * @package app\wechat\command
 */
class Clear extends Command
{
    protected function configure()
    {
        $this->setName('xadmin:fanspay');
        $this->setDescription('Wechat Users Payment auto clear for ThinkAdmin');
    }

    /**
     * 执行支付单清理任务
     * @param \think\console\Input $input
     * @param \think\console\Output $output
     * @throws \think\admin\Exception
     * @throws \think\db\exception\DbException
     */
    protected function execute(Input $input, Output $output)
    {
        $query = SysWechatPaymentRecord::mq();
        $query->where(['payment_status' => 0]);
        $query->whereTime('create_time', '<', strtotime('-24 hours'));
        [$total, $count] = [(clone $query)->count(), 0];
        if (empty($total)) $this->setQueueSuccess('无需清理24小时未支付！');
        /** @var \think\Model $item */
        foreach ($query->cursor() as $item) {
            $this->setQueueMessage($total, ++$count, sprintf('开始清理 %s 支付单...', $item->getAttr('code')));
            $item->delete();
            $this->setQueueMessage($total, $count, sprintf('完成清理 %s 支付单！', $item->getAttr('code')), 1);
        }
    }
}