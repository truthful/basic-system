<?php

use think\migration\Migrator;

@set_time_limit(0);
@ini_set('memory_limit', -1);

class InstallSysTable extends Migrator {

	/**
	 * 创建数据库
	 */
	 public function change() {
		$this->_create_sys_article();
		$this->_create_sys_article_category();
		$this->_create_sys_block_chain();
		$this->_create_sys_config();
		$this->_create_sys_data();
		$this->_create_sys_dict();
		$this->_create_sys_email();
		$this->_create_sys_file();
		$this->_create_sys_file_group();
		$this->_create_sys_gpc();
		$this->_create_sys_industry();
		$this->_create_sys_menu();
		$this->_create_sys_merchant();
		$this->_create_sys_message();
		$this->_create_sys_mobile_menu();
		$this->_create_sys_module();
		$this->_create_sys_oplog();
		$this->_create_sys_public_category();
		$this->_create_sys_queue();
		$this->_create_sys_region();
		$this->_create_sys_relation();
		$this->_create_sys_role();
		$this->_create_sys_sms();
		$this->_create_sys_spa();
		$this->_create_sys_standard_code();
		$this->_create_sys_user();
		$this->_create_sys_wechat_auth();
		$this->_create_sys_wechat_auto();
		$this->_create_sys_wechat_fans();
		$this->_create_sys_wechat_fans_tags();
		$this->_create_sys_wechat_keys();
		$this->_create_sys_wechat_media();
		$this->_create_sys_wechat_news();
		$this->_create_sys_wechat_news_article();
		$this->_create_sys_wechat_payment_record();
		$this->_create_sys_wechat_payment_refund();
		$this->_create_sys_world_code();

	}

    /**
     * 创建数据对象
     * @class SysArticle
     * @table sys_article
     * @return void
     */
    private function _create_sys_article() {

        // 当前数据表
        $table = 'sys_article';

        // 存在则跳过
        if ($this->hasTable($table)) return;

        // 创建数据表
        $this->table($table, [
            'engine' => 'InnoDB', 'collation' => 'utf8mb4_general_ci', 'comment' => '系统-文章记录表',
        ])
		->addColumn('title','string',['limit' => 300, 'default' => '', 'null' => false, 'comment' => '文章标题'])
		->addColumn('show_type','integer',['default' => '10', 'null' => false, 'comment' => '列表显示方式(10小图展示 20大图展示)'])
		->addColumn('cate_id','integer',['default' => '0', 'null' => false, 'comment' => '文章分类ID'])
		->addColumn('image','string',['limit' => 500, 'default' => NULL, 'null' => false, 'comment' => '封面图'])
		->addColumn('content','text',['default' => NULL, 'null' => false, 'comment' => '文章内容'])
		->addColumn('sort','integer',['default' => '0', 'null' => false, 'comment' => '文章排序(数字越小越靠前)'])
		->addColumn('status','integer',['default' => '0', 'null' => false, 'comment' => '文章状态(0隐藏 1显示)'])
		->addColumn('virtual_views','integer',['default' => '0', 'null' => false, 'comment' => '虚拟阅读量(仅用作展示)'])
		->addColumn('actual_views','integer',['default' => '0', 'null' => false, 'comment' => '实际阅读量'])
		->addColumn('is_deleted','integer',['default' => '0', 'null' => false, 'comment' => '是否删除'])
		->addColumn('create_time','datetime',['default' => 'CURRENT_TIMESTAMP', 'null' => false, 'comment' => '创建时间'])
		->addColumn('created_by','biginteger',['default' => '0', 'null' => false, 'comment' => '创建者'])
		->addColumn('update_time','datetime',['default' => 'CURRENT_TIMESTAMP', 'null' => false, 'comment' => '更新时间'])
		->addColumn('updated_by','biginteger',['default' => '0', 'null' => false, 'comment' => '更新者'])
		->addIndex('cate_id', ['name' => 'idx_sys_article_cate_id'])
		->addIndex('status', ['name' => 'idx_sys_article_status'])
		->addIndex('is_deleted', ['name' => 'idx_sys_article_is_deleted'])
		->addIndex('create_time', ['name' => 'idx_sys_article_create_time'])
		->create();

		// 修改主键长度
		$this->table($table)->changeColumn('id','integer',['limit'=>11,'identity'=>true]);
	}

    /**
     * 创建数据对象
     * @class SysArticleCategory
     * @table sys_article_category
     * @return void
     */
    private function _create_sys_article_category() {

        // 当前数据表
        $table = 'sys_article_category';

        // 存在则跳过
        if ($this->hasTable($table)) return;

        // 创建数据表
        $this->table($table, [
            'engine' => 'InnoDB', 'collation' => 'utf8mb4_general_ci', 'comment' => '系统-文章分类表',
        ])
		->addColumn('name','string',['limit' => 50, 'default' => '', 'null' => false, 'comment' => '分类名称'])
		->addColumn('status','integer',['default' => '0', 'null' => false, 'comment' => '状态(1显示 0隐藏)'])
		->addColumn('is_deleted','integer',['default' => '0', 'null' => false, 'comment' => '删除状态(0正常 1隐藏)'])
		->addColumn('sort','integer',['default' => '0', 'null' => false, 'comment' => '排序方式(数字越小越靠前)'])
		->addColumn('create_time','datetime',['default' => 'CURRENT_TIMESTAMP', 'null' => false, 'comment' => '创建时间'])
		->addColumn('created_by','biginteger',['default' => '0', 'null' => false, 'comment' => '创建者'])
		->addColumn('update_time','datetime',['default' => 'CURRENT_TIMESTAMP', 'null' => false, 'comment' => '更新时间'])
		->addColumn('updated_by','biginteger',['default' => '0', 'null' => false, 'comment' => '更新者'])
		->addIndex('status', ['name' => 'idx_sys_article_category_status'])
		->addIndex('is_deleted', ['name' => 'idx_sys_article_category_is_deleted'])
		->addIndex('create_time', ['name' => 'idx_sys_article_category_create_time'])
		->create();

		// 修改主键长度
		$this->table($table)->changeColumn('id','integer',['limit'=>11,'identity'=>true]);
	}

    /**
     * 创建数据对象
     * @class SysBlockChain
     * @table sys_block_chain
     * @return void
     */
    private function _create_sys_block_chain() {

        // 当前数据表
        $table = 'sys_block_chain';

        // 存在则跳过
        if ($this->hasTable($table)) return;

        // 创建数据表
        $this->table($table, [
            'engine' => 'InnoDB', 'collation' => 'utf8mb4_general_ci', 'comment' => '系统-区块链上链纪录',
        ])
		->addColumn('merchant_id','string',['limit' => 36, 'default' => NULL, 'null' => true, 'comment' => '商户标识'])
		->addColumn('block_number','biginteger',['default' => '0', 'null' => true, 'comment' => '所在区块'])
		->addColumn('chain_type','string',['limit' => 20, 'default' => 'DEPOSIT', 'null' => true, 'comment' => '上链类型 DEPOSIT 存证 DEPLOYWASMCONTRACT合约'])
		->addColumn('hash','string',['limit' => 100, 'default' => NULL, 'null' => true, 'comment' => 'HASH值'])
		->addColumn('md5_value','string',['limit' => 32, 'default' => NULL, 'null' => true, 'comment' => '上链内容MD5'])
		->addColumn('code','integer',['default' => '0', 'null' => true, 'comment' => '上链状态'])
		->addColumn('data_type','string',['limit' => 20, 'default' => NULL, 'null' => true, 'comment' => '上链数据类型（原料入库，生产入库，入库，出库，质检，农事）'])
		->addColumn('content','text',['default' => NULL, 'null' => true, 'comment' => '摘要'])
		->addColumn('gas_used','biginteger',['default' => '0', 'null' => true, 'comment' => '消费数量'])
		->addColumn('qrcode','string',['limit' => 255, 'default' => NULL, 'null' => true, 'comment' => '二维码'])
		->addColumn('remark','string',['limit' => 255, 'default' => NULL, 'null' => true, 'comment' => '备注'])
		->addColumn('status','integer',['default' => '0', 'null' => true, 'comment' => '上链状态'])
		->addColumn('sort','integer',['default' => '100', 'null' => true, 'comment' => '排序'])
		->addColumn('is_deleted','integer',['default' => '0', 'null' => false, 'comment' => '删除状态'])
		->addColumn('create_time','datetime',['default' => 'CURRENT_TIMESTAMP', 'null' => true, 'comment' => '创建时间'])
		->addColumn('created_by','biginteger',['default' => '0', 'null' => true, 'comment' => '创建人'])
		->addIndex('hash', ['name' => 'idx_sys_block_chain_hash'])
		->addIndex('created_by', ['name' => 'idx_sys_block_chain_created_by'])
		->addIndex('data_type', ['name' => 'idx_sys_block_chain_data_type'])
		->addIndex('status', ['name' => 'idx_sys_block_chain_status'])
		->addIndex('is_deleted', ['name' => 'idx_sys_block_chain_is_deleted'])
		->addIndex('create_time', ['name' => 'idx_sys_block_chain_create_time'])
		->create();

		// 修改主键长度
		$this->table($table)->changeColumn('id','integer',['limit'=>11,'identity'=>true]);
	}

    /**
     * 创建数据对象
     * @class SysConfig
     * @table sys_config
     * @return void
     */
    private function _create_sys_config() {

        // 当前数据表
        $table = 'sys_config';

        // 存在则跳过
        if ($this->hasTable($table)) return;

        // 创建数据表
        $this->table($table, [
            'engine' => 'InnoDB', 'collation' => 'utf8mb4_general_ci', 'comment' => '系统-配置',
        ])
		->addColumn('config_key','string',['limit' => 255, 'default' => NULL, 'null' => true, 'comment' => '配置键'])
		->addColumn('config_value','text',['default' => NULL, 'null' => true, 'comment' => '配置值'])
		->addColumn('category','string',['limit' => 255, 'default' => NULL, 'null' => true, 'comment' => '分类'])
		->addColumn('remark','string',['limit' => 255, 'default' => NULL, 'null' => true, 'comment' => '备注'])
		->addColumn('sort','integer',['default' => '99', 'null' => true, 'comment' => '排序码'])
		->addColumn('ext_json','text',['default' => NULL, 'null' => true, 'comment' => '扩展信息'])
		->addColumn('status','integer',['default' => '0', 'null' => false, 'comment' => '状态（字典 0正常 1停用 2删除）'])
		->addColumn('is_deleted','integer',['default' => '0', 'null' => false, 'comment' => '状态（字典 0正常 1删除）'])
		->addColumn('create_time','datetime',['default' => 'CURRENT_TIMESTAMP', 'null' => true, 'comment' => '创建时间'])
		->addColumn('created_by','biginteger',['default' => NULL, 'null' => true, 'comment' => '创建人'])
		->addColumn('update_time','datetime',['default' => NULL, 'null' => true, 'comment' => '修改时间'])
		->addColumn('updated_by','biginteger',['default' => NULL, 'null' => true, 'comment' => '修改人'])
		->addIndex('category', ['name' => 'idx_sys_config_category'])
		->addIndex('status', ['name' => 'idx_sys_config_status'])
		->addIndex('is_deleted', ['name' => 'idx_sys_config_is_deleted'])
		->create();

		// 修改主键长度
		$this->table($table)->changeColumn('id','integer',['limit'=>11,'identity'=>true]);
	}

    /**
     * 创建数据对象
     * @class SysData
     * @table sys_data
     * @return void
     */
    private function _create_sys_data() {

        // 当前数据表
        $table = 'sys_data';

        // 存在则跳过
        if ($this->hasTable($table)) return;

        // 创建数据表
        $this->table($table, [
            'engine' => 'InnoDB', 'collation' => 'utf8mb4_general_ci', 'comment' => '系统-数据',
        ])
		->addColumn('name','string',['limit' => 100, 'default' => '', 'null' => true, 'comment' => '配置名'])
		->addColumn('value','text',['default' => NULL, 'null' => true, 'comment' => '配置值'])
		->addColumn('create_time','datetime',['default' => NULL, 'null' => true, 'comment' => '创建时间'])
		->addColumn('update_time','datetime',['default' => NULL, 'null' => true, 'comment' => '更新时间'])
		->addIndex('name', ['name' => 'idx_sys_data_name'])
		->addIndex('create_time', ['name' => 'idx_sys_data_create_time'])
		->create();

		// 修改主键长度
		$this->table($table)->changeColumn('id','integer',['limit'=>11,'identity'=>true]);
	}

    /**
     * 创建数据对象
     * @class SysDict
     * @table sys_dict
     * @return void
     */
    private function _create_sys_dict() {

        // 当前数据表
        $table = 'sys_dict';

        // 存在则跳过
        if ($this->hasTable($table)) return;

        // 创建数据表
        $this->table($table, [
            'engine' => 'InnoDB', 'collation' => 'utf8mb4_general_ci', 'comment' => '系统-字典',
        ])
		->addColumn('parent_id','biginteger',['default' => '0', 'null' => true, 'comment' => '父ID'])
		->addColumn('dict_label','string',['limit' => 255, 'default' => NULL, 'null' => true, 'comment' => '字典文字'])
		->addColumn('dict_value','string',['limit' => 255, 'default' => NULL, 'null' => true, 'comment' => '字典值'])
		->addColumn('category','string',['limit' => 255, 'default' => NULL, 'null' => true, 'comment' => '分类'])
		->addColumn('sort','integer',['default' => NULL, 'null' => true, 'comment' => '排序码'])
		->addColumn('ext_json','text',['default' => NULL, 'null' => true, 'comment' => '扩展信息'])
		->addColumn('status','integer',['default' => '0', 'null' => false, 'comment' => '状态（字典 0正常 1停用 2删除）'])
		->addColumn('is_deleted','integer',['default' => '0', 'null' => false, 'comment' => '状态（字典 0正常 1删除）'])
		->addColumn('create_time','datetime',['default' => 'CURRENT_TIMESTAMP', 'null' => true, 'comment' => '创建时间'])
		->addColumn('created_by','biginteger',['default' => NULL, 'null' => true, 'comment' => '创建人'])
		->addColumn('update_time','datetime',['default' => NULL, 'null' => true, 'comment' => '修改时间'])
		->addColumn('updated_by','biginteger',['default' => NULL, 'null' => true, 'comment' => '修改人'])
		->addIndex('parent_id', ['name' => 'idx_sys_dict_parent_id'])
		->addIndex('category', ['name' => 'idx_sys_dict_category'])
		->addIndex('status', ['name' => 'idx_sys_dict_status'])
		->addIndex('is_deleted', ['name' => 'idx_sys_dict_is_deleted'])
		->create();

		// 修改主键长度
		$this->table($table)->changeColumn('id','integer',['limit'=>11,'identity'=>true]);
	}

    /**
     * 创建数据对象
     * @class SysEmail
     * @table sys_email
     * @return void
     */
    private function _create_sys_email() {

        // 当前数据表
        $table = 'sys_email';

        // 存在则跳过
        if ($this->hasTable($table)) return;

        // 创建数据表
        $this->table($table, [
            'engine' => 'InnoDB', 'collation' => 'utf8mb4_general_ci', 'comment' => '系统-邮件发送表',
        ])
		->addColumn('engine','string',['limit' => 255, 'default' => NULL, 'null' => true, 'comment' => '邮件引擎'])
		->addColumn('send_account','string',['limit' => 255, 'default' => NULL, 'null' => true, 'comment' => '发件人邮箱'])
		->addColumn('send_user','string',['limit' => 255, 'default' => NULL, 'null' => true, 'comment' => '发件人昵称'])
		->addColumn('receive_accounts','text',['default' => NULL, 'null' => true, 'comment' => '接收人邮箱'])
		->addColumn('subject','string',['limit' => 255, 'default' => NULL, 'null' => true, 'comment' => '邮件主题'])
		->addColumn('content','text',['default' => NULL, 'null' => true, 'comment' => '邮件内容'])
		->addColumn('tag_name','string',['limit' => 255, 'default' => NULL, 'null' => true, 'comment' => '标签名称'])
		->addColumn('template_name','text',['default' => NULL, 'null' => true, 'comment' => '模板名称'])
		->addColumn('template_param','text',['default' => NULL, 'null' => true, 'comment' => '发送参数'])
		->addColumn('code','string',['limit' => 255, 'default' => NULL, 'null' => true, 'comment' => '参数数字'])
		->addColumn('receipt_info','text',['default' => NULL, 'null' => true, 'comment' => '回执信息'])
		->addColumn('ext_json','text',['default' => NULL, 'null' => true, 'comment' => '扩展信息'])
		->addColumn('status','integer',['default' => '0', 'null' => false, 'comment' => '邮件状态(0失败,1成功)'])
		->addColumn('is_deleted','integer',['default' => '0', 'null' => false, 'comment' => '状态（字典 0正常 1删除）'])
		->addColumn('create_time','datetime',['default' => 'CURRENT_TIMESTAMP', 'null' => true, 'comment' => '创建时间'])
		->addColumn('created_by','biginteger',['default' => NULL, 'null' => true, 'comment' => '创建人'])
		->addColumn('update_time','datetime',['default' => NULL, 'null' => true, 'comment' => '修改时间'])
		->addColumn('updated_by','biginteger',['default' => NULL, 'null' => true, 'comment' => '修改人'])
		->addIndex('status', ['name' => 'idx_sys_email_status'])
		->addIndex('is_deleted', ['name' => 'idx_sys_email_is_deleted'])
		->addIndex('create_time', ['name' => 'idx_sys_email_create_time'])
		->create();

		// 修改主键长度
		$this->table($table)->changeColumn('id','integer',['limit'=>11,'identity'=>true]);
	}

    /**
     * 创建数据对象
     * @class SysFile
     * @table sys_file
     * @return void
     */
    private function _create_sys_file() {

        // 当前数据表
        $table = 'sys_file';

        // 存在则跳过
        if ($this->hasTable($table)) return;

        // 创建数据表
        $this->table($table, [
            'engine' => 'InnoDB', 'collation' => 'utf8mb4_general_ci', 'comment' => '系统-文件',
        ])
		->addColumn('group_id','biginteger',['default' => '0', 'null' => true, 'comment' => '文件组ID'])
		->addColumn('engine','string',['limit' => 255, 'default' => NULL, 'null' => true, 'comment' => '存储引擎'])
		->addColumn('bucket','string',['limit' => 255, 'default' => NULL, 'null' => true, 'comment' => '存储桶'])
		->addColumn('name','text',['default' => NULL, 'null' => true, 'comment' => '文件名称'])
		->addColumn('suffix','string',['limit' => 255, 'default' => NULL, 'null' => true, 'comment' => '文件后缀'])
		->addColumn('size_kb','biginteger',['default' => NULL, 'null' => true, 'comment' => '文件大小kb'])
		->addColumn('size_info','string',['limit' => 255, 'default' => NULL, 'null' => true, 'comment' => '文件大小（格式化后）'])
		->addColumn('obj_name','text',['default' => NULL, 'null' => true, 'comment' => '文件的对象名（唯一名称）'])
		->addColumn('storage_path','text',['default' => NULL, 'null' => true, 'comment' => '文件存储路径'])
		->addColumn('download_path','text',['default' => NULL, 'null' => true, 'comment' => '文件下载路径'])
		->addColumn('thumbnail','text',['default' => NULL, 'null' => true, 'comment' => '图片缩略图'])
		->addColumn('ext_json','text',['default' => NULL, 'null' => true, 'comment' => '扩展信息'])
		->addColumn('status','integer',['default' => '0', 'null' => true, 'comment' => '状态(0失败,1成功)'])
		->addColumn('is_deleted','integer',['default' => '0', 'null' => false, 'comment' => '状态（字典 0正常 1删除）'])
		->addColumn('create_time','datetime',['default' => 'CURRENT_TIMESTAMP', 'null' => true, 'comment' => '创建时间'])
		->addColumn('created_by','biginteger',['default' => NULL, 'null' => true, 'comment' => '创建人'])
		->addColumn('update_time','datetime',['default' => NULL, 'null' => true, 'comment' => '修改时间'])
		->addColumn('updated_by','biginteger',['default' => NULL, 'null' => true, 'comment' => '修改人'])
		->addIndex('group_id', ['name' => 'idx_sys_file_group_id'])
		->addIndex('status', ['name' => 'idx_sys_file_status'])
		->addIndex('is_deleted', ['name' => 'idx_sys_file_is_deleted'])
		->create();

		// 修改主键长度
		$this->table($table)->changeColumn('id','integer',['limit'=>11,'identity'=>true]);
	}

    /**
     * 创建数据对象
     * @class SysFileGroup
     * @table sys_file_group
     * @return void
     */
    private function _create_sys_file_group() {

        // 当前数据表
        $table = 'sys_file_group';

        // 存在则跳过
        if ($this->hasTable($table)) return;

        // 创建数据表
        $this->table($table, [
            'engine' => 'InnoDB', 'collation' => 'utf8mb4_general_ci', 'comment' => '系统-文件库分组记录表',
        ])
		->addColumn('name','string',['limit' => 30, 'default' => '', 'null' => false, 'comment' => '分组名称'])
		->addColumn('parent_id','biginteger',['default' => '0', 'null' => false, 'comment' => '上级分组ID'])
		->addColumn('sort','integer',['default' => '100', 'null' => false, 'comment' => '排序(数字越小越靠前)'])
		->addColumn('status','integer',['default' => '0', 'null' => true, 'comment' => ''])
		->addColumn('is_deleted','integer',['default' => '0', 'null' => true, 'comment' => '0正常1删除'])
		->addColumn('created_by','biginteger',['default' => NULL, 'null' => true, 'comment' => '创建人'])
		->addColumn('updated_by','biginteger',['default' => NULL, 'null' => true, 'comment' => '修改人'])
		->addColumn('create_time','datetime',['default' => 'CURRENT_TIMESTAMP', 'null' => true, 'comment' => '创建时间'])
		->addColumn('update_time','datetime',['default' => NULL, 'null' => true, 'comment' => '修改时间'])
		->addIndex('parent_id', ['name' => 'idx_sys_file_group_parent_id'])
		->addIndex('status', ['name' => 'idx_sys_file_group_status'])
		->addIndex('is_deleted', ['name' => 'idx_sys_file_group_is_deleted'])
		->create();

		// 修改主键长度
		$this->table($table)->changeColumn('id','integer',['limit'=>11,'identity'=>true]);
	}

    /**
     * 创建数据对象
     * @class SysGpc
     * @table sys_gpc
     * @return void
     */
    private function _create_sys_gpc() {

        // 当前数据表
        $table = 'sys_gpc';

        // 存在则跳过
        if ($this->hasTable($table)) return;

        // 创建数据表
        $this->table($table, [
            'engine' => 'InnoDB', 'collation' => 'utf8mb4_general_ci', 'comment' => '系统-GPC分类',
        ])
		->addColumn('gpc_id','string',['limit' => 36, 'default' => NULL, 'null' => true, 'comment' => '标识'])
		->addColumn('parent_id','biginteger',['default' => '0', 'null' => true, 'comment' => '父ID'])
		->addColumn('code','string',['limit' => 255, 'default' => NULL, 'null' => true, 'comment' => '编码'])
		->addColumn('description','string',['limit' => 255, 'default' => NULL, 'null' => true, 'comment' => '中文名称'])
		->addColumn('description_en','string',['limit' => 255, 'default' => NULL, 'null' => true, 'comment' => '英文名称'])
		->addColumn('hierarchy','biginteger',['default' => NULL, 'null' => true, 'comment' => '等级'])
		->addColumn('child_node','text',['default' => NULL, 'null' => true, 'comment' => '子节点'])
		->addColumn('sort','integer',['default' => '100', 'null' => true, 'comment' => '排序码'])
		->addColumn('status','integer',['default' => '0', 'null' => true, 'comment' => '状态(0失败,1成功)'])
		->addColumn('is_deleted','integer',['default' => '0', 'null' => true, 'comment' => '删除状态（字典 0正常 1删除）'])
		->addColumn('create_time','datetime',['default' => 'CURRENT_TIMESTAMP', 'null' => true, 'comment' => '创建时间'])
		->addColumn('created_by','biginteger',['default' => NULL, 'null' => true, 'comment' => '创建人'])
		->addColumn('update_time','datetime',['default' => NULL, 'null' => true, 'comment' => '修改时间'])
		->addColumn('updated_by','biginteger',['default' => NULL, 'null' => true, 'comment' => '修改人'])
		->addIndex('status', ['name' => 'idx_sys_gpc_status'])
		->addIndex('parent_id', ['name' => 'idx_sys_gpc_parent_id'])
		->addIndex('is_deleted', ['name' => 'idx_sys_gpc_is_deleted'])
		->create();

		// 修改主键长度
		$this->table($table)->changeColumn('id','integer',['limit'=>11,'identity'=>true]);
	}

    /**
     * 创建数据对象
     * @class SysIndustry
     * @table sys_industry
     * @return void
     */
    private function _create_sys_industry() {

        // 当前数据表
        $table = 'sys_industry';

        // 存在则跳过
        if ($this->hasTable($table)) return;

        // 创建数据表
        $this->table($table, [
            'engine' => 'InnoDB', 'collation' => 'utf8mb4_general_ci', 'comment' => '系统-行业-分类',
        ])
		->addColumn('industry_id','string',['limit' => 36, 'default' => NULL, 'null' => true, 'comment' => '标识'])
		->addColumn('parent_id','integer',['default' => '0', 'null' => true, 'comment' => '上级行业分类'])
		->addColumn('title','string',['limit' => 255, 'default' => '', 'null' => true, 'comment' => '行业分类名称'])
		->addColumn('desc','string',['limit' => 1024, 'default' => '', 'null' => true, 'comment' => '行业分类描述'])
		->addColumn('standard','integer',['default' => '0', 'null' => false, 'comment' => '是否有国家标准'])
		->addColumn('standard_no','string',['limit' => 255, 'default' => '', 'null' => true, 'comment' => '标准号'])
		->addColumn('standard_file','string',['limit' => 255, 'default' => '', 'null' => true, 'comment' => '国标文件'])
		->addColumn('status','char',['limit' => 1, 'default' => '0', 'null' => true, 'comment' => '状态'])
		->addColumn('sort','biginteger',['default' => '0', 'null' => true, 'comment' => '排序权重'])
		->addColumn('is_deleted','integer',['default' => '0', 'null' => false, 'comment' => '删除状态'])
		->addColumn('create_time','datetime',['default' => 'CURRENT_TIMESTAMP', 'null' => false, 'comment' => '创建时间'])
		->addColumn('created_by','biginteger',['default' => '0', 'null' => false, 'comment' => '创者人'])
		->addColumn('update_time','datetime',['default' => 'CURRENT_TIMESTAMP', 'null' => false, 'comment' => '更新时间'])
		->addColumn('updated_by','biginteger',['default' => '0', 'null' => false, 'comment' => '更新人'])
		->addIndex('parent_id', ['name' => 'idx_sys_industry_parent_id'])
		->addIndex('is_deleted', ['name' => 'idx_sys_industry_is_deleted'])
		->addIndex('status', ['name' => 'idx_sys_industry_status'])
		->create();

		// 修改主键长度
		$this->table($table)->changeColumn('id','integer',['limit'=>11,'identity'=>true]);
	}

    /**
     * 创建数据对象
     * @class SysMenu
     * @table sys_menu
     * @return void
     */
    private function _create_sys_menu() {

        // 当前数据表
        $table = 'sys_menu';

        // 存在则跳过
        if ($this->hasTable($table)) return;

        // 创建数据表
        $this->table($table, [
            'engine' => 'InnoDB', 'collation' => 'utf8mb4_general_ci', 'comment' => '系统-菜单',
        ])
		->addColumn('parent_id','biginteger',['default' => '0', 'null' => true, 'comment' => '父ID'])
		->addColumn('group_name','string',['limit' => 30, 'default' => NULL, 'null' => true, 'comment' => '分组名称'])
		->addColumn('title','string',['limit' => 255, 'default' => NULL, 'null' => true, 'comment' => '标题'])
		->addColumn('name','string',['limit' => 255, 'default' => NULL, 'null' => true, 'comment' => '别名'])
		->addColumn('code','string',['limit' => 255, 'default' => NULL, 'null' => true, 'comment' => '编码(api)'])
		->addColumn('category','string',['limit' => 255, 'default' => NULL, 'null' => true, 'comment' => '分类'])
		->addColumn('package','string',['limit' => 255, 'default' => NULL, 'null' => true, 'comment' => '子系统'])
		->addColumn('module','integer',['default' => NULL, 'null' => true, 'comment' => '模块ID'])
		->addColumn('menu_type','string',['limit' => 255, 'default' => NULL, 'null' => true, 'comment' => '菜单类型'])
		->addColumn('path','text',['default' => NULL, 'null' => true, 'comment' => '路径'])
		->addColumn('component','string',['limit' => 255, 'default' => NULL, 'null' => true, 'comment' => '组件'])
		->addColumn('icon','string',['limit' => 255, 'default' => NULL, 'null' => true, 'comment' => '图标'])
		->addColumn('is_hidden','integer',['default' => '0', 'null' => true, 'comment' => '是否隐藏1隐藏0显示'])
		->addColumn('sort','integer',['default' => NULL, 'null' => true, 'comment' => '排序码'])
		->addColumn('status','integer',['default' => '0', 'null' => true, 'comment' => '邮件状态(0失败,1成功)'])
		->addColumn('is_deleted','integer',['default' => '0', 'null' => true, 'comment' => '状态（字典 0正常 1删除）'])
		->addColumn('create_time','datetime',['default' => 'CURRENT_TIMESTAMP', 'null' => true, 'comment' => '创建时间'])
		->addColumn('created_by','biginteger',['default' => NULL, 'null' => true, 'comment' => '创建人'])
		->addColumn('update_time','datetime',['default' => NULL, 'null' => true, 'comment' => '修改时间'])
		->addColumn('updated_by','biginteger',['default' => NULL, 'null' => true, 'comment' => '修改人'])
		->addIndex('is_deleted', ['name' => 'idx_sys_menu_is_deleted'])
		->addIndex('status', ['name' => 'idx_sys_menu_status'])
		->addIndex('create_time', ['name' => 'idx_sys_menu_create_time'])
		->addIndex('parent_id', ['name' => 'idx_sys_menu_parent_id'])
		->create();

		// 修改主键长度
		$this->table($table)->changeColumn('id','integer',['limit'=>11,'identity'=>true]);
	}

    /**
     * 创建数据对象
     * @class SysMerchant
     * @table sys_merchant
     * @return void
     */
    private function _create_sys_merchant() {

        // 当前数据表
        $table = 'sys_merchant';

        // 存在则跳过
        if ($this->hasTable($table)) return;

        // 创建数据表
        $this->table($table, [
            'engine' => 'InnoDB', 'collation' => 'utf8mb4_general_ci', 'comment' => '系统-商户',
        ])
		->addColumn('merchant_id','string',['limit' => 36, 'default' => NULL, 'null' => true, 'comment' => '商户标识'])
		->addColumn('merchant_code','string',['limit' => 36, 'default' => NULL, 'null' => true, 'comment' => '商户编码'])
		->addColumn('name','string',['limit' => 50, 'default' => NULL, 'null' => true, 'comment' => '商户名称'])
		->addColumn('sub_name','string',['limit' => 255, 'default' => NULL, 'null' => true, 'comment' => '子行业名称'])
		->addColumn('package','string',['limit' => 255, 'default' => NULL, 'null' => true, 'comment' => '子系统名称'])
		->addColumn('auth_status','string',['limit' => 50, 'default' => 'NOT_AUTH', 'null' => true, 'comment' => '商户认证状态（未认证NOT_AUTH｜认证成功AUTH_SUCCESS｜企业认证成功ENTERPRISE_AUTH_SUCCESS｜个人认证成功INDIVIDUAL_AUTH_SUCCESS｜认证失败/待完善AUTH_FAIL｜审核中UNDER_REVIEW）'])
		->addColumn('auth_type','string',['limit' => 20, 'default' => 'COMPANY', 'null' => true, 'comment' => '商户认证方式'])
		->addColumn('province','string',['limit' => 30, 'default' => NULL, 'null' => true, 'comment' => '省'])
		->addColumn('city','string',['limit' => 30, 'default' => NULL, 'null' => true, 'comment' => '市'])
		->addColumn('county','string',['limit' => 30, 'default' => NULL, 'null' => true, 'comment' => '区'])
		->addColumn('company_name','string',['limit' => 80, 'default' => NULL, 'null' => true, 'comment' => '企业名称'])
		->addColumn('uniform_social_credit_code','string',['limit' => 30, 'default' => NULL, 'null' => true, 'comment' => '统一社会信用代码'])
		->addColumn('person_name','string',['limit' => 30, 'default' => NULL, 'null' => true, 'comment' => '个人或法人姓名'])
		->addColumn('identity_card','string',['limit' => 20, 'default' => NULL, 'null' => true, 'comment' => '身份证号'])
		->addColumn('has_signed_agreement','integer',['default' => '1', 'null' => true, 'comment' => '是否已经签署用户协议(1未签 0已签)'])
		->addColumn('info_type','string',['limit' => 20, 'default' => 'NORMAL', 'null' => true, 'comment' => '信息录入规范'])
		->addColumn('is_self','integer',['default' => '0', 'null' => true, 'comment' => '是否为自有商户（字典 0是 1否）'])
		->addColumn('gas_total','biginteger',['default' => '1000000', 'null' => true, 'comment' => 'gas总量'])
		->addColumn('gas_used','biginteger',['default' => '0', 'null' => true, 'comment' => 'gas使用量'])
		->addColumn('remark','text',['default' => NULL, 'null' => true, 'comment' => '备注'])
		->addColumn('sort','integer',['default' => NULL, 'null' => true, 'comment' => '排序码'])
		->addColumn('ext_json','text',['default' => NULL, 'null' => true, 'comment' => '扩展信息'])
		->addColumn('status','integer',['default' => '0', 'null' => true, 'comment' => '邮件状态(0失败,1成功)'])
		->addColumn('is_deleted','integer',['default' => '0', 'null' => true, 'comment' => '状态（字典 0正常 1删除）'])
		->addColumn('create_time','datetime',['default' => 'CURRENT_TIMESTAMP', 'null' => true, 'comment' => '创建时间'])
		->addColumn('created_by','biginteger',['default' => NULL, 'null' => true, 'comment' => '创建人'])
		->addColumn('update_time','datetime',['default' => NULL, 'null' => true, 'comment' => '修改时间'])
		->addColumn('updated_by','biginteger',['default' => NULL, 'null' => true, 'comment' => '修改人'])
		->addIndex('merchant_id', ['name' => 'idx_sys_merchant_merchant_id'])
		->addIndex('auth_status', ['name' => 'idx_sys_merchant_auth_status'])
		->addIndex('status', ['name' => 'idx_sys_merchant_status'])
		->addIndex('sub_name', ['name' => 'idx_sys_merchant_sub_name'])
		->addIndex('is_deleted', ['name' => 'idx_sys_merchant_is_deleted'])
		->create();

		// 修改主键长度
		$this->table($table)->changeColumn('id','integer',['limit'=>11,'identity'=>true]);
	}

    /**
     * 创建数据对象
     * @class SysMessage
     * @table sys_message
     * @return void
     */
    private function _create_sys_message() {

        // 当前数据表
        $table = 'sys_message';

        // 存在则跳过
        if ($this->hasTable($table)) return;

        // 创建数据表
        $this->table($table, [
            'engine' => 'InnoDB', 'collation' => 'utf8mb4_general_ci', 'comment' => '系统-站内信',
        ])
		->addColumn('category','string',['limit' => 255, 'default' => NULL, 'null' => true, 'comment' => '分类'])
		->addColumn('subject','text',['default' => NULL, 'null' => true, 'comment' => '主题'])
		->addColumn('content','text',['default' => NULL, 'null' => true, 'comment' => '正文'])
		->addColumn('ext_json','text',['default' => NULL, 'null' => true, 'comment' => '扩展信息'])
		->addColumn('status','integer',['default' => '0', 'null' => false, 'comment' => '状态（字典 0正常 1停用 2删除）'])
		->addColumn('is_deleted','integer',['default' => '0', 'null' => false, 'comment' => '状态（字典 0正常 1删除）'])
		->addColumn('create_time','datetime',['default' => 'CURRENT_TIMESTAMP', 'null' => true, 'comment' => '创建时间'])
		->addColumn('created_by','biginteger',['default' => NULL, 'null' => true, 'comment' => '创建人'])
		->addColumn('update_time','datetime',['default' => NULL, 'null' => true, 'comment' => '修改时间'])
		->addColumn('updated_by','biginteger',['default' => NULL, 'null' => true, 'comment' => '修改人'])
		->addIndex('category', ['name' => 'idx_sys_message_category'])
		->addIndex('status', ['name' => 'idx_sys_message_status'])
		->addIndex('is_deleted', ['name' => 'idx_sys_message_is_deleted'])
		->create();

		// 修改主键长度
		$this->table($table)->changeColumn('id','integer',['limit'=>11,'identity'=>true]);
	}

    /**
     * 创建数据对象
     * @class SysMobileMenu
     * @table sys_mobile_menu
     * @return void
     */
    private function _create_sys_mobile_menu() {

        // 当前数据表
        $table = 'sys_mobile_menu';

        // 存在则跳过
        if ($this->hasTable($table)) return;

        // 创建数据表
        $this->table($table, [
            'engine' => 'InnoDB', 'collation' => 'utf8mb4_general_ci', 'comment' => '客户端菜单',
        ])
		->addColumn('parent_id','string',['limit' => 255, 'default' => NULL, 'null' => true, 'comment' => '父ID'])
		->addColumn('title','string',['limit' => 255, 'default' => NULL, 'null' => true, 'comment' => '名称'])
		->addColumn('code','string',['limit' => 255, 'default' => NULL, 'null' => true, 'comment' => '编码'])
		->addColumn('category','string',['limit' => 255, 'default' => NULL, 'null' => true, 'comment' => '分类'])
		->addColumn('module','biginteger',['default' => '0', 'null' => true, 'comment' => '模块ID'])
		->addColumn('menu_type','string',['limit' => 255, 'default' => NULL, 'null' => true, 'comment' => '菜单类型'])
		->addColumn('path','string',['limit' => 255, 'default' => NULL, 'null' => true, 'comment' => '路径'])
		->addColumn('icon','string',['limit' => 255, 'default' => NULL, 'null' => true, 'comment' => '图标'])
		->addColumn('color','string',['limit' => 255, 'default' => NULL, 'null' => true, 'comment' => '颜色'])
		->addColumn('reg_type','string',['limit' => 255, 'default' => NULL, 'null' => true, 'comment' => '规则类型'])
		->addColumn('sort','integer',['default' => NULL, 'null' => true, 'comment' => '排序码'])
		->addColumn('ext_json','text',['default' => NULL, 'null' => true, 'comment' => '扩展信息'])
		->addColumn('status','integer',['default' => '0', 'null' => true, 'comment' => '状态(0失败,1成功)'])
		->addColumn('is_deleted','integer',['default' => '0', 'null' => true, 'comment' => '状态（字典 0正常 1删除）'])
		->addColumn('create_time','datetime',['default' => 'CURRENT_TIMESTAMP', 'null' => true, 'comment' => '创建时间'])
		->addColumn('created_by','biginteger',['default' => NULL, 'null' => true, 'comment' => '创建人'])
		->addColumn('update_time','datetime',['default' => NULL, 'null' => true, 'comment' => '修改时间'])
		->addColumn('updated_by','biginteger',['default' => NULL, 'null' => true, 'comment' => '修改人'])
		->addIndex('parent_id', ['name' => 'idx_sys_mobile_menu_parent_id'])
		->addIndex('code', ['name' => 'idx_sys_mobile_menu_code'])
		->addIndex('module', ['name' => 'idx_sys_mobile_menu_module'])
		->addIndex('status', ['name' => 'idx_sys_mobile_menu_status'])
		->addIndex('is_deleted', ['name' => 'idx_sys_mobile_menu_is_deleted'])
		->create();

		// 修改主键长度
		$this->table($table)->changeColumn('id','integer',['limit'=>11,'identity'=>true]);
	}

    /**
     * 创建数据对象
     * @class SysModule
     * @table sys_module
     * @return void
     */
    private function _create_sys_module() {

        // 当前数据表
        $table = 'sys_module';

        // 存在则跳过
        if ($this->hasTable($table)) return;

        // 创建数据表
        $this->table($table, [
            'engine' => 'InnoDB', 'collation' => 'utf8mb4_general_ci', 'comment' => '系统-模块',
        ])
		->addColumn('module_id','string',['limit' => 36, 'default' => NULL, 'null' => true, 'comment' => '子系统标识'])
		->addColumn('title','string',['limit' => 255, 'default' => NULL, 'null' => true, 'comment' => '名称'])
		->addColumn('color','string',['limit' => 255, 'default' => NULL, 'null' => true, 'comment' => '颜色'])
		->addColumn('icon','string',['limit' => 255, 'default' => NULL, 'null' => true, 'comment' => 'icon'])
		->addColumn('code','string',['limit' => 255, 'default' => NULL, 'null' => true, 'comment' => '编码'])
		->addColumn('category','string',['limit' => 255, 'default' => NULL, 'null' => true, 'comment' => '分类'])
		->addColumn('sort','integer',['default' => NULL, 'null' => true, 'comment' => '排序码'])
		->addColumn('ext_json','text',['default' => NULL, 'null' => true, 'comment' => '扩展信息'])
		->addColumn('status','integer',['default' => '0', 'null' => true, 'comment' => '邮件状态(0失败,1成功)'])
		->addColumn('is_deleted','integer',['default' => '0', 'null' => true, 'comment' => '状态（字典 0正常 1删除）'])
		->addColumn('create_time','datetime',['default' => 'CURRENT_TIMESTAMP', 'null' => true, 'comment' => '创建时间'])
		->addColumn('created_by','biginteger',['default' => NULL, 'null' => true, 'comment' => '创建人'])
		->addColumn('update_time','datetime',['default' => NULL, 'null' => true, 'comment' => '修改时间'])
		->addColumn('updated_by','biginteger',['default' => NULL, 'null' => true, 'comment' => '修改人'])
		->addIndex('is_deleted', ['name' => 'idx_sys_module_is_deleted'])
		->addIndex('status', ['name' => 'idx_sys_module_status'])
		->addIndex('create_time', ['name' => 'idx_sys_module_create_time'])
		->create();

		// 修改主键长度
		$this->table($table)->changeColumn('id','integer',['limit'=>11,'identity'=>true]);
	}

    /**
     * 创建数据对象
     * @class SysOplog
     * @table sys_oplog
     * @return void
     */
    private function _create_sys_oplog() {

        // 当前数据表
        $table = 'sys_oplog';

        // 存在则跳过
        if ($this->hasTable($table)) return;

        // 创建数据表
        $this->table($table, [
            'engine' => 'InnoDB', 'collation' => 'utf8mb4_general_ci', 'comment' => '系统-日志',
        ])
		->addColumn('node','string',['limit' => 200, 'default' => '', 'null' => false, 'comment' => '当前操作节点'])
		->addColumn('geoip','string',['limit' => 15, 'default' => '', 'null' => false, 'comment' => '操作者IP地址'])
		->addColumn('action','string',['limit' => 200, 'default' => '', 'null' => false, 'comment' => '操作行为名称'])
		->addColumn('content','string',['limit' => 1024, 'default' => '', 'null' => false, 'comment' => '操作内容描述'])
		->addColumn('username','string',['limit' => 50, 'default' => '', 'null' => false, 'comment' => '操作人用户名'])
		->addColumn('create_time','datetime',['default' => 'CURRENT_TIMESTAMP', 'null' => false, 'comment' => '创建时间'])
		->addIndex('create_time', ['name' => 'idx_sys_oplog_create_time'])
		->create();

		// 修改主键长度
		$this->table($table)->changeColumn('id','integer',['limit'=>11,'identity'=>true]);
	}

    /**
     * 创建数据对象
     * @class SysPublicCategory
     * @table sys_public_category
     * @return void
     */
    private function _create_sys_public_category() {

        // 当前数据表
        $table = 'sys_public_category';

        // 存在则跳过
        if ($this->hasTable($table)) return;

        // 创建数据表
        $this->table($table, [
            'engine' => 'InnoDB', 'collation' => 'utf8mb4_general_ci', 'comment' => '系统-商品公共分类表',
        ])
		->addColumn('goods_cate_id','string',['limit' => 36, 'default' => NULL, 'null' => true, 'comment' => '标识'])
		->addColumn('name','string',['limit' => 50, 'default' => '', 'null' => false, 'comment' => '分类名称'])
		->addColumn('parent_id','integer',['default' => '0', 'null' => false, 'comment' => '上级分类'])
		->addColumn('level','integer',['default' => '0', 'null' => false, 'comment' => '分类级别'])
		->addColumn('status','integer',['default' => '0', 'null' => false, 'comment' => '状态(1显示 0隐藏)'])
		->addColumn('is_deleted','integer',['default' => '0', 'null' => false, 'comment' => '删除状态(0正常 1隐藏)'])
		->addColumn('sort','integer',['default' => '0', 'null' => false, 'comment' => '排序方式(数字越小越靠前)'])
		->addColumn('create_time','datetime',['default' => 'CURRENT_TIMESTAMP', 'null' => false, 'comment' => '创建时间'])
		->addColumn('created_by','biginteger',['default' => '0', 'null' => false, 'comment' => '创建者'])
		->addColumn('update_time','datetime',['default' => 'CURRENT_TIMESTAMP', 'null' => false, 'comment' => '更新时间'])
		->addColumn('updated_by','biginteger',['default' => '0', 'null' => false, 'comment' => '更新者'])
		->create();

		// 修改主键长度
		$this->table($table)->changeColumn('id','integer',['limit'=>11,'identity'=>true]);
	}

    /**
     * 创建数据对象
     * @class SysQueue
     * @table sys_queue
     * @return void
     */
    private function _create_sys_queue() {

        // 当前数据表
        $table = 'sys_queue';

        // 存在则跳过
        if ($this->hasTable($table)) return;

        // 创建数据表
        $this->table($table, [
            'engine' => 'InnoDB', 'collation' => 'utf8mb4_general_ci', 'comment' => '系统-任务',
        ])
		->addColumn('code','string',['limit' => 20, 'default' => '', 'null' => false, 'comment' => '任务编号'])
		->addColumn('title','string',['limit' => 100, 'default' => '', 'null' => false, 'comment' => '任务名称'])
		->addColumn('command','string',['limit' => 500, 'default' => '', 'null' => true, 'comment' => '执行指令'])
		->addColumn('exec_pid','biginteger',['default' => '0', 'null' => true, 'comment' => '执行进程'])
		->addColumn('exec_data','text',['default' => NULL, 'null' => true, 'comment' => '执行参数'])
		->addColumn('exec_time','biginteger',['default' => '0', 'null' => true, 'comment' => '执行时间'])
		->addColumn('exec_desc','string',['limit' => 500, 'default' => '', 'null' => true, 'comment' => '执行描述'])
		->addColumn('enter_time','decimal',['precision' => 20, 'scale' => 4, 'default' => '0.0000', 'null' => true, 'comment' => '开始时间'])
		->addColumn('outer_time','decimal',['precision' => 20, 'scale' => 4, 'default' => '0.0000', 'null' => true, 'comment' => '结束时间'])
		->addColumn('loops_time','biginteger',['default' => '0', 'null' => true, 'comment' => '循环时间'])
		->addColumn('attempts','biginteger',['default' => '0', 'null' => true, 'comment' => '执行次数'])
		->addColumn('message','text',['default' => NULL, 'null' => true, 'comment' => '最新消息'])
		->addColumn('rscript','integer',['default' => '1', 'null' => true, 'comment' => '任务类型(0单例,1多例)'])
		->addColumn('status','integer',['default' => '1', 'null' => true, 'comment' => '任务状态(1新任务,2处理中,3成功,4失败)'])
		->addColumn('create_time','datetime',['default' => 'CURRENT_TIMESTAMP', 'null' => false, 'comment' => '创建时间'])
		->addColumn('update_time','datetime',['default' => NULL, 'null' => true, 'comment' => '更新时间'])
		->addIndex('code', ['name' => 'idx_sys_queue_code'])
		->addIndex('title', ['name' => 'idx_sys_queue_title'])
		->addIndex('status', ['name' => 'idx_sys_queue_status'])
		->addIndex('rscript', ['name' => 'idx_sys_queue_rscript'])
		->addIndex('create_time', ['name' => 'idx_sys_queue_create_time'])
		->addIndex('exec_time', ['name' => 'idx_sys_queue_exec_time'])
		->create();

		// 修改主键长度
		$this->table($table)->changeColumn('id','integer',['limit'=>11,'identity'=>true]);
	}

    /**
     * 创建数据对象
     * @class SysRegion
     * @table sys_region
     * @return void
     */
    private function _create_sys_region() {

        // 当前数据表
        $table = 'sys_region';

        // 存在则跳过
        if ($this->hasTable($table)) return;

        // 创建数据表
        $this->table($table, [
            'engine' => 'InnoDB', 'collation' => 'utf8mb4_general_ci', 'comment' => '数据-区域',
        ])
		->addColumn('region_id','string',['limit' => 36, 'default' => NULL, 'null' => true, 'comment' => '标识'])
		->addColumn('pid','biginteger',['default' => NULL, 'null' => true, 'comment' => '上级PID'])
		->addColumn('first','string',['limit' => 50, 'default' => '', 'null' => true, 'comment' => '首字母'])
		->addColumn('short','string',['limit' => 100, 'default' => '', 'null' => true, 'comment' => '区域简称'])
		->addColumn('name','string',['limit' => 100, 'default' => '', 'null' => true, 'comment' => '区域名称'])
		->addColumn('level','integer',['default' => '0', 'null' => true, 'comment' => '区域层级'])
		->addColumn('pinyin','string',['limit' => 100, 'default' => '', 'null' => true, 'comment' => '区域拼音'])
		->addColumn('code','string',['limit' => 100, 'default' => '', 'null' => true, 'comment' => '区域邮编'])
		->addColumn('status','integer',['default' => '1', 'null' => true, 'comment' => '使用状态'])
		->addColumn('lng','string',['limit' => 100, 'default' => '', 'null' => true, 'comment' => '所在经度'])
		->addColumn('lat','string',['limit' => 100, 'default' => '', 'null' => true, 'comment' => '所在纬度'])
		->addColumn('create_time','datetime',['default' => NULL, 'null' => true, 'comment' => '创建时间'])
		->addIndex('pid', ['name' => 'idx_sys_region_pid'])
		->addIndex('name', ['name' => 'idx_sys_region_name'])
		->create();

		// 修改主键长度
		$this->table($table)->changeColumn('id','integer',['limit'=>11,'identity'=>true]);
	}

    /**
     * 创建数据对象
     * @class SysRelation
     * @table sys_relation
     * @return void
     */
    private function _create_sys_relation() {

        // 当前数据表
        $table = 'sys_relation';

        // 存在则跳过
        if ($this->hasTable($table)) return;

        // 创建数据表
        $this->table($table, [
            'engine' => 'InnoDB', 'collation' => 'utf8mb4_general_ci', 'comment' => '系统-关系',
        ])
		->addColumn('object_id','string',['limit' => 255, 'default' => '0', 'null' => false, 'comment' => '对象ID'])
		->addColumn('target_id','string',['limit' => 255, 'default' => '0', 'null' => false, 'comment' => '目标ID'])
		->addColumn('category','string',['limit' => 255, 'default' => NULL, 'null' => true, 'comment' => '分类'])
		->addColumn('ext_json','text',['default' => NULL, 'null' => true, 'comment' => '扩展信息'])
		->addColumn('create_time','datetime',['default' => NULL, 'null' => true, 'comment' => '创建时间'])
		->addIndex('category', ['name' => 'idx_sys_relation_category'])
		->create();

		// 修改主键长度
		$this->table($table)->changeColumn('id','integer',['limit'=>11,'identity'=>true]);
	}

    /**
     * 创建数据对象
     * @class SysRole
     * @table sys_role
     * @return void
     */
    private function _create_sys_role() {

        // 当前数据表
        $table = 'sys_role';

        // 存在则跳过
        if ($this->hasTable($table)) return;

        // 创建数据表
        $this->table($table, [
            'engine' => 'InnoDB', 'collation' => 'utf8mb4_general_ci', 'comment' => '系统-角色',
        ])
		->addColumn('org_id','string',['limit' => 20, 'default' => NULL, 'null' => true, 'comment' => '组织id'])
		->addColumn('name','string',['limit' => 255, 'default' => NULL, 'null' => true, 'comment' => '名称'])
		->addColumn('code','string',['limit' => 255, 'default' => NULL, 'null' => true, 'comment' => '编码'])
		->addColumn('category','string',['limit' => 255, 'default' => NULL, 'null' => true, 'comment' => '分类'])
		->addColumn('sort','integer',['default' => NULL, 'null' => true, 'comment' => '排序码'])
		->addColumn('ext_json','text',['default' => NULL, 'null' => true, 'comment' => '扩展信息'])
		->addColumn('status','integer',['default' => '0', 'null' => true, 'comment' => '邮件状态(0失败,1成功)'])
		->addColumn('is_deleted','integer',['default' => '0', 'null' => false, 'comment' => '状态（字典 0正常 1删除）'])
		->addColumn('create_time','datetime',['default' => 'CURRENT_TIMESTAMP', 'null' => true, 'comment' => '创建时间'])
		->addColumn('created_by','biginteger',['default' => NULL, 'null' => true, 'comment' => '创建人'])
		->addColumn('update_time','datetime',['default' => NULL, 'null' => true, 'comment' => '修改时间'])
		->addColumn('updated_by','biginteger',['default' => NULL, 'null' => true, 'comment' => '修改人'])
		->create();

		// 修改主键长度
		$this->table($table)->changeColumn('id','integer',['limit'=>11,'identity'=>true]);
	}

    /**
     * 创建数据对象
     * @class SysSms
     * @table sys_sms
     * @return void
     */
    private function _create_sys_sms() {

        // 当前数据表
        $table = 'sys_sms';

        // 存在则跳过
        if ($this->hasTable($table)) return;

        // 创建数据表
        $this->table($table, [
            'engine' => 'InnoDB', 'collation' => 'utf8mb4_general_ci', 'comment' => '系统-短信发送表',
        ])
		->addColumn('engine','string',['limit' => 255, 'default' => NULL, 'null' => true, 'comment' => '短信引擎'])
		->addColumn('phone_numbers','text',['default' => NULL, 'null' => true, 'comment' => '手机号'])
		->addColumn('sign_name','text',['default' => NULL, 'null' => true, 'comment' => '短信签名'])
		->addColumn('template_code','text',['default' => NULL, 'null' => true, 'comment' => '模板编码'])
		->addColumn('template_param','text',['default' => NULL, 'null' => true, 'comment' => '发送参数'])
		->addColumn('receipt_info','text',['default' => NULL, 'null' => true, 'comment' => '回执信息'])
		->addColumn('ext_json','text',['default' => NULL, 'null' => true, 'comment' => '扩展信息'])
		->addColumn('status','integer',['default' => '0', 'null' => true, 'comment' => '短信状态(0失败,1成功)'])
		->addColumn('is_deleted','integer',['default' => '0', 'null' => false, 'comment' => '状态（字典 0正常 1删除）'])
		->addColumn('create_time','datetime',['default' => 'CURRENT_TIMESTAMP', 'null' => true, 'comment' => '创建时间'])
		->addColumn('created_by','biginteger',['default' => NULL, 'null' => true, 'comment' => '创建人'])
		->addColumn('update_time','datetime',['default' => NULL, 'null' => true, 'comment' => '修改时间'])
		->addColumn('updated_by','biginteger',['default' => NULL, 'null' => true, 'comment' => '修改人'])
		->addIndex('engine', ['name' => 'idx_sys_sms_engine'])
		->addIndex('status', ['name' => 'idx_sys_sms_status'])
		->create();

		// 修改主键长度
		$this->table($table)->changeColumn('id','integer',['limit'=>11,'identity'=>true]);
	}

    /**
     * 创建数据对象
     * @class SysSpa
     * @table sys_spa
     * @return void
     */
    private function _create_sys_spa() {

        // 当前数据表
        $table = 'sys_spa';

        // 存在则跳过
        if ($this->hasTable($table)) return;

        // 创建数据表
        $this->table($table, [
            'engine' => 'InnoDB', 'collation' => 'utf8mb4_general_ci', 'comment' => '系统-单页',
        ])
		->addColumn('title','string',['limit' => 255, 'default' => NULL, 'null' => true, 'comment' => '标题'])
		->addColumn('name','string',['limit' => 255, 'default' => NULL, 'null' => true, 'comment' => '名称'])
		->addColumn('group_name','string',['limit' => 30, 'default' => NULL, 'null' => true, 'comment' => '组名称'])
		->addColumn('menu_type','string',['limit' => 255, 'default' => NULL, 'null' => true, 'comment' => '类型'])
		->addColumn('component','string',['limit' => 255, 'default' => NULL, 'null' => true, 'comment' => ''])
		->addColumn('module','string',['limit' => 255, 'default' => NULL, 'null' => true, 'comment' => '关联子系统（模块）'])
		->addColumn('icon','string',['limit' => 255, 'default' => NULL, 'null' => true, 'comment' => 'icon'])
		->addColumn('code','string',['limit' => 255, 'default' => NULL, 'null' => true, 'comment' => '编码'])
		->addColumn('path','string',['limit' => 255, 'default' => NULL, 'null' => true, 'comment' => '路径'])
		->addColumn('category','string',['limit' => 255, 'default' => NULL, 'null' => true, 'comment' => '分类'])
		->addColumn('sort','integer',['default' => NULL, 'null' => true, 'comment' => '排序码'])
		->addColumn('is_top','integer',['default' => NULL, 'null' => true, 'comment' => '是否置顶'])
		->addColumn('is_hidden','integer',['default' => NULL, 'null' => true, 'comment' => '是否隐藏'])
		->addColumn('remark','string',['limit' => 255, 'default' => NULL, 'null' => true, 'comment' => '备注'])
		->addColumn('ext_json','text',['default' => NULL, 'null' => true, 'comment' => '扩展信息'])
		->addColumn('status','integer',['default' => '0', 'null' => false, 'comment' => '邮件状态(0失败,1成功)'])
		->addColumn('is_deleted','integer',['default' => '0', 'null' => false, 'comment' => '状态（字典 0正常 1删除）'])
		->addColumn('create_time','datetime',['default' => 'CURRENT_TIMESTAMP', 'null' => true, 'comment' => '创建时间'])
		->addColumn('created_by','biginteger',['default' => NULL, 'null' => true, 'comment' => '创建人'])
		->addColumn('update_time','datetime',['default' => NULL, 'null' => true, 'comment' => '修改时间'])
		->addColumn('updated_by','biginteger',['default' => NULL, 'null' => true, 'comment' => '修改人'])
		->create();

		// 修改主键长度
		$this->table($table)->changeColumn('id','integer',['limit'=>11,'identity'=>true]);
	}

    /**
     * 创建数据对象
     * @class SysStandardCode
     * @table sys_standard_code
     * @return void
     */
    private function _create_sys_standard_code() {

        // 当前数据表
        $table = 'sys_standard_code';

        // 存在则跳过
        if ($this->hasTable($table)) return;

        // 创建数据表
        $this->table($table, [
            'engine' => 'InnoDB', 'collation' => 'utf8mb4_general_ci', 'comment' => '系统-标准代码',
        ])
		->addColumn('standard_id','string',['limit' => 36, 'default' => NULL, 'null' => true, 'comment' => '标识'])
		->addColumn('parent_id','biginteger',['default' => NULL, 'null' => true, 'comment' => '上级ID'])
		->addColumn('group_name','string',['limit' => 255, 'default' => NULL, 'null' => true, 'comment' => '分组名称'])
		->addColumn('source','string',['limit' => 255, 'default' => NULL, 'null' => true, 'comment' => '来源'])
		->addColumn('code','string',['limit' => 255, 'default' => NULL, 'null' => true, 'comment' => '代码'])
		->addColumn('en_value','string',['limit' => 255, 'default' => NULL, 'null' => true, 'comment' => '英文名'])
		->addColumn('cn_value','string',['limit' => 255, 'default' => NULL, 'null' => true, 'comment' => '中文名'])
		->addColumn('desc','string',['limit' => 255, 'default' => NULL, 'null' => true, 'comment' => '说明'])
		->addColumn('ext_json','text',['default' => NULL, 'null' => true, 'comment' => '扩展信息'])
		->addColumn('sort','integer',['default' => '100', 'null' => true, 'comment' => '排序'])
		->addColumn('status','integer',['default' => '0', 'null' => false, 'comment' => '状态（字典 0正常 1停用 2删除）'])
		->addColumn('is_deleted','integer',['default' => '0', 'null' => false, 'comment' => '状态（字典 0正常 1删除）'])
		->addColumn('create_time','datetime',['default' => 'CURRENT_TIMESTAMP', 'null' => true, 'comment' => '创建时间'])
		->addColumn('created_by','biginteger',['default' => NULL, 'null' => true, 'comment' => '创建人'])
		->addColumn('update_time','datetime',['default' => NULL, 'null' => true, 'comment' => '修改时间'])
		->addColumn('updated_by','biginteger',['default' => NULL, 'null' => true, 'comment' => '修改人'])
		->addIndex('parent_id', ['name' => 'idx_sys_standard_code_parent_id'])
		->addIndex('group_name', ['name' => 'idx_sys_standard_code_group_name'])
		->addIndex('source', ['name' => 'idx_sys_standard_code_source'])
		->addIndex('code', ['name' => 'idx_sys_standard_code_code'])
		->addIndex('status', ['name' => 'idx_sys_standard_code_status'])
		->addIndex('is_deleted', ['name' => 'idx_sys_standard_code_is_deleted'])
		->create();

		// 修改主键长度
		$this->table($table)->changeColumn('id','integer',['limit'=>11,'identity'=>true]);
	}

    /**
     * 创建数据对象
     * @class SysUser
     * @table sys_user
     * @return void
     */
    private function _create_sys_user() {

        // 当前数据表
        $table = 'sys_user';

        // 存在则跳过
        if ($this->hasTable($table)) return;

        // 创建数据表
        $this->table($table, [
            'engine' => 'InnoDB', 'collation' => 'utf8mb4_general_ci', 'comment' => '系统-用户',
        ])
		->addColumn('avatar','text',['default' => NULL, 'null' => true, 'comment' => '头像'])
		->addColumn('account','string',['limit' => 255, 'default' => NULL, 'null' => true, 'comment' => '账号'])
		->addColumn('password','string',['limit' => 255, 'default' => NULL, 'null' => true, 'comment' => '密码'])
		->addColumn('name','string',['limit' => 255, 'default' => NULL, 'null' => true, 'comment' => '姓名'])
		->addColumn('nickname','string',['limit' => 255, 'default' => NULL, 'null' => true, 'comment' => '昵称'])
		->addColumn('gender','string',['limit' => 255, 'default' => NULL, 'null' => true, 'comment' => '性别'])
		->addColumn('category','string',['limit' => 20, 'default' => NULL, 'null' => true, 'comment' => '分类'])
		->addColumn('age','string',['limit' => 255, 'default' => NULL, 'null' => true, 'comment' => '年龄'])
		->addColumn('birthday','string',['limit' => 255, 'default' => NULL, 'null' => true, 'comment' => '出生日期'])
		->addColumn('phone','string',['limit' => 255, 'default' => NULL, 'null' => true, 'comment' => '手机'])
		->addColumn('email','string',['limit' => 255, 'default' => NULL, 'null' => true, 'comment' => '邮箱'])
		->addColumn('office_tel','string',['limit' => 255, 'default' => NULL, 'null' => true, 'comment' => '办公电话'])
		->addColumn('login_num','biginteger',['default' => '0', 'null' => true, 'comment' => '登录次数'])
		->addColumn('latest_login_ip','string',['limit' => 255, 'default' => NULL, 'null' => true, 'comment' => '最新登录ip'])
		->addColumn('latest_login_address','string',['limit' => 255, 'default' => NULL, 'null' => true, 'comment' => '最新登录地点'])
		->addColumn('latest_login_time','datetime',['default' => NULL, 'null' => true, 'comment' => '最新登录时间'])
		->addColumn('latest_login_device','text',['default' => NULL, 'null' => true, 'comment' => '最新登录设备'])
		->addColumn('sort','integer',['default' => NULL, 'null' => true, 'comment' => '排序码'])
		->addColumn('ext_json','text',['default' => NULL, 'null' => true, 'comment' => '扩展信息'])
		->addColumn('status','integer',['default' => '0', 'null' => true, 'comment' => '邮件状态(0失败,1成功)'])
		->addColumn('is_deleted','integer',['default' => '0', 'null' => false, 'comment' => '状态（字典 0正常 1删除）'])
		->addColumn('create_time','datetime',['default' => NULL, 'null' => true, 'comment' => '创建时间'])
		->addColumn('created_by','biginteger',['default' => NULL, 'null' => true, 'comment' => '创建人'])
		->addColumn('update_time','datetime',['default' => NULL, 'null' => true, 'comment' => '修改时间'])
		->addColumn('updated_by','biginteger',['default' => '0', 'null' => true, 'comment' => '修改人'])
		->create();

		// 修改主键长度
		$this->table($table)->changeColumn('id','integer',['limit'=>11,'identity'=>true]);
	}

    /**
     * 创建数据对象
     * @class SysWechatAuth
     * @table sys_wechat_auth
     * @return void
     */
    private function _create_sys_wechat_auth() {

        // 当前数据表
        $table = 'sys_wechat_auth';

        // 存在则跳过
        if ($this->hasTable($table)) return;

        // 创建数据表
        $this->table($table, [
            'engine' => 'InnoDB', 'collation' => 'utf8mb4_general_ci', 'comment' => '微信-授权',
        ])
		->addColumn('authorizer_appid','string',['limit' => 100, 'default' => '', 'null' => true, 'comment' => '微信APPID'])
		->addColumn('authorizer_access_token','string',['limit' => 200, 'default' => '', 'null' => true, 'comment' => '授权Token'])
		->addColumn('authorizer_refresh_token','string',['limit' => 200, 'default' => '', 'null' => true, 'comment' => '刷新Token'])
		->addColumn('expires_in','integer',['default' => '0', 'null' => true, 'comment' => 'Token时限'])
		->addColumn('user_alias','string',['limit' => 100, 'default' => '', 'null' => true, 'comment' => '公众号别名'])
		->addColumn('user_name','string',['limit' => 100, 'default' => '', 'null' => true, 'comment' => '众众号原账号'])
		->addColumn('user_nickname','string',['limit' => 100, 'default' => '', 'null' => true, 'comment' => '公众号昵称'])
		->addColumn('user_headimg','string',['limit' => 200, 'default' => '', 'null' => true, 'comment' => '公众号头像'])
		->addColumn('user_signature','string',['limit' => 200, 'default' => '', 'null' => true, 'comment' => '公众号描述'])
		->addColumn('user_company','string',['limit' => 200, 'default' => '', 'null' => true, 'comment' => '公众号公司'])
		->addColumn('func_info','string',['limit' => 100, 'default' => '', 'null' => true, 'comment' => '公众号集权'])
		->addColumn('service_type','string',['limit' => 10, 'default' => '', 'null' => true, 'comment' => '公众号类型'])
		->addColumn('service_verify','string',['limit' => 10, 'default' => '', 'null' => true, 'comment' => '公众号认证'])
		->addColumn('qrcode_url','string',['limit' => 200, 'default' => '', 'null' => true, 'comment' => '公众号二维码'])
		->addColumn('businessinfo','string',['limit' => 500, 'default' => '', 'null' => true, 'comment' => '业务序列内容'])
		->addColumn('miniprograminfo','string',['limit' => 500, 'default' => '', 'null' => true, 'comment' => '小程序序列内容'])
		->addColumn('total','integer',['default' => '0', 'null' => true, 'comment' => '统计调用次数'])
		->addColumn('appkey','string',['limit' => 32, 'default' => '', 'null' => true, 'comment' => '应用接口KEY'])
		->addColumn('appuri','string',['limit' => 255, 'default' => '', 'null' => true, 'comment' => '应用接口URI'])
		->addColumn('status','integer',['default' => '1', 'null' => true, 'comment' => '授权状态(0已取消,1已授权)'])
		->addColumn('deleted','integer',['default' => '0', 'null' => true, 'comment' => '删除状态(0未删除,1已删除)'])
		->addColumn('auth_time','integer',['default' => '0', 'null' => true, 'comment' => '授权时间'])
		->addColumn('create_time','datetime',['default' => 'CURRENT_TIMESTAMP', 'null' => true, 'comment' => '创建时间'])
		->addIndex('authorizer_appid', ['name' => 'idx_sys_wechat_auth_authorizer_appid'])
		->addIndex('status', ['name' => 'idx_sys_wechat_auth_status'])
		->addIndex('deleted', ['name' => 'idx_sys_wechat_auth_deleted'])
		->create();

		// 修改主键长度
		$this->table($table)->changeColumn('id','integer',['limit'=>11,'identity'=>true]);
	}

    /**
     * 创建数据对象
     * @class SysWechatAuto
     * @table sys_wechat_auto
     * @return void
     */
    private function _create_sys_wechat_auto() {

        // 当前数据表
        $table = 'sys_wechat_auto';

        // 存在则跳过
        if ($this->hasTable($table)) return;

        // 创建数据表
        $this->table($table, [
            'engine' => 'InnoDB', 'collation' => 'utf8mb4_general_ci', 'comment' => '微信-回复',
        ])
		->addColumn('type','string',['limit' => 20, 'default' => '', 'null' => true, 'comment' => '类型(text,image,news)'])
		->addColumn('time','string',['limit' => 100, 'default' => '', 'null' => true, 'comment' => '延迟时间'])
		->addColumn('code','string',['limit' => 20, 'default' => '', 'null' => true, 'comment' => '消息编号'])
		->addColumn('appid','string',['limit' => 100, 'default' => '', 'null' => true, 'comment' => '公众号APPID'])
		->addColumn('content','text',['default' => NULL, 'null' => true, 'comment' => '文本内容'])
		->addColumn('image_url','string',['limit' => 255, 'default' => '', 'null' => true, 'comment' => '图片链接'])
		->addColumn('voice_url','string',['limit' => 255, 'default' => '', 'null' => true, 'comment' => '语音链接'])
		->addColumn('music_title','string',['limit' => 100, 'default' => '', 'null' => true, 'comment' => '音乐标题'])
		->addColumn('music_url','string',['limit' => 255, 'default' => '', 'null' => true, 'comment' => '音乐链接'])
		->addColumn('music_image','string',['limit' => 255, 'default' => '', 'null' => true, 'comment' => '缩略图片'])
		->addColumn('music_desc','string',['limit' => 255, 'default' => '', 'null' => true, 'comment' => '音乐描述'])
		->addColumn('video_title','string',['limit' => 100, 'default' => '', 'null' => true, 'comment' => '视频标题'])
		->addColumn('video_url','string',['limit' => 255, 'default' => '', 'null' => true, 'comment' => '视频URL'])
		->addColumn('video_desc','string',['limit' => 255, 'default' => '', 'null' => true, 'comment' => '视频描述'])
		->addColumn('news_id','biginteger',['default' => '0', 'null' => true, 'comment' => '图文ID'])
		->addColumn('status','integer',['default' => '1', 'null' => true, 'comment' => '状态(0禁用,1启用)'])
		->addColumn('created_by','biginteger',['default' => '0', 'null' => true, 'comment' => '创建人'])
		->addColumn('create_time','datetime',['default' => 'CURRENT_TIMESTAMP', 'null' => true, 'comment' => '创建时间'])
		->addIndex('code', ['name' => 'idx_sys_wechat_auto_code'])
		->addIndex('type', ['name' => 'idx_sys_wechat_auto_type'])
		->addIndex('time', ['name' => 'idx_sys_wechat_auto_time'])
		->addIndex('appid', ['name' => 'idx_sys_wechat_auto_appid'])
		->addIndex('status', ['name' => 'idx_sys_wechat_auto_status'])
		->create();

		// 修改主键长度
		$this->table($table)->changeColumn('id','integer',['limit'=>11,'identity'=>true]);
	}

    /**
     * 创建数据对象
     * @class SysWechatFans
     * @table sys_wechat_fans
     * @return void
     */
    private function _create_sys_wechat_fans() {

        // 当前数据表
        $table = 'sys_wechat_fans';

        // 存在则跳过
        if ($this->hasTable($table)) return;

        // 创建数据表
        $this->table($table, [
            'engine' => 'InnoDB', 'collation' => 'utf8mb4_general_ci', 'comment' => '微信-粉丝',
        ])
		->addColumn('appid','string',['limit' => 50, 'default' => '', 'null' => true, 'comment' => '公众号APPID'])
		->addColumn('unionid','string',['limit' => 100, 'default' => '', 'null' => true, 'comment' => '粉丝unionid'])
		->addColumn('openid','string',['limit' => 100, 'default' => '', 'null' => true, 'comment' => '粉丝openid'])
		->addColumn('tagid_list','string',['limit' => 100, 'default' => '', 'null' => true, 'comment' => '粉丝标签id'])
		->addColumn('is_black','integer',['default' => '0', 'null' => true, 'comment' => '是否为黑名单状态'])
		->addColumn('subscribe','integer',['default' => '0', 'null' => true, 'comment' => '关注状态(0未关注,1已关注)'])
		->addColumn('nickname','string',['limit' => 200, 'default' => '', 'null' => true, 'comment' => '用户昵称'])
		->addColumn('sex','integer',['default' => '0', 'null' => true, 'comment' => '用户性别(1男性,2女性,0未知)'])
		->addColumn('country','string',['limit' => 50, 'default' => '', 'null' => true, 'comment' => '用户所在国家'])
		->addColumn('province','string',['limit' => 50, 'default' => '', 'null' => true, 'comment' => '用户所在省份'])
		->addColumn('city','string',['limit' => 50, 'default' => '', 'null' => true, 'comment' => '用户所在城市'])
		->addColumn('language','string',['limit' => 50, 'default' => '', 'null' => true, 'comment' => '用户的语言(zh_CN)'])
		->addColumn('headimgurl','string',['limit' => 500, 'default' => '', 'null' => true, 'comment' => '用户头像'])
		->addColumn('subscribe_time','biginteger',['default' => '0', 'null' => true, 'comment' => '关注时间'])
		->addColumn('subscribe_at','datetime',['default' => NULL, 'null' => true, 'comment' => '关注时间'])
		->addColumn('remark','string',['limit' => 50, 'default' => '', 'null' => true, 'comment' => '备注'])
		->addColumn('subscribe_scene','string',['limit' => 200, 'default' => '', 'null' => true, 'comment' => '扫码关注场景'])
		->addColumn('qr_scene','string',['limit' => 100, 'default' => '', 'null' => true, 'comment' => '二维码场景值'])
		->addColumn('qr_scene_str','string',['limit' => 200, 'default' => '', 'null' => true, 'comment' => '二维码场景内容'])
		->addColumn('create_time','datetime',['default' => 'CURRENT_TIMESTAMP', 'null' => true, 'comment' => '创建时间'])
		->addIndex('appid', ['name' => 'idx_sys_wechat_fans_appid'])
		->addIndex('openid', ['name' => 'idx_sys_wechat_fans_openid'])
		->addIndex('unionid', ['name' => 'idx_sys_wechat_fans_unionid'])
		->addIndex('is_black', ['name' => 'idx_sys_wechat_fans_is_black'])
		->addIndex('subscribe', ['name' => 'idx_sys_wechat_fans_subscribe'])
		->create();

		// 修改主键长度
		$this->table($table)->changeColumn('id','integer',['limit'=>11,'identity'=>true]);
	}

    /**
     * 创建数据对象
     * @class SysWechatFansTags
     * @table sys_wechat_fans_tags
     * @return void
     */
    private function _create_sys_wechat_fans_tags() {

        // 当前数据表
        $table = 'sys_wechat_fans_tags';

        // 存在则跳过
        if ($this->hasTable($table)) return;

        // 创建数据表
        $this->table($table, [
            'engine' => 'InnoDB', 'collation' => 'utf8mb4_general_ci', 'comment' => '微信-标签',
        ])
		->addColumn('appid','string',['limit' => 50, 'default' => '', 'null' => true, 'comment' => '公众号APPID'])
		->addColumn('name','string',['limit' => 35, 'default' => '', 'null' => true, 'comment' => '标签名称'])
		->addColumn('count','biginteger',['default' => '0', 'null' => true, 'comment' => '粉丝总数'])
		->addColumn('create_time','datetime',['default' => 'CURRENT_TIMESTAMP', 'null' => true, 'comment' => '创建日期'])
		->addIndex('id', ['name' => 'idx_sys_wechat_fans_tags_id'])
		->addIndex('appid', ['name' => 'idx_sys_wechat_fans_tags_appid'])
		->create();

		// 修改主键长度
		$this->table($table)->changeColumn('id','integer',['limit'=>11,'identity'=>true]);
	}

    /**
     * 创建数据对象
     * @class SysWechatKeys
     * @table sys_wechat_keys
     * @return void
     */
    private function _create_sys_wechat_keys() {

        // 当前数据表
        $table = 'sys_wechat_keys';

        // 存在则跳过
        if ($this->hasTable($table)) return;

        // 创建数据表
        $this->table($table, [
            'engine' => 'InnoDB', 'collation' => 'utf8mb4_general_ci', 'comment' => '微信-规则',
        ])
		->addColumn('appid','string',['limit' => 100, 'default' => '', 'null' => true, 'comment' => '公众号APPID'])
		->addColumn('type','string',['limit' => 20, 'default' => '', 'null' => true, 'comment' => '类型(text,image,news)'])
		->addColumn('keys','string',['limit' => 100, 'default' => '', 'null' => true, 'comment' => '关键字'])
		->addColumn('content','text',['default' => NULL, 'null' => true, 'comment' => '文本内容'])
		->addColumn('image_url','string',['limit' => 255, 'default' => '', 'null' => true, 'comment' => '图片链接'])
		->addColumn('voice_url','string',['limit' => 255, 'default' => '', 'null' => true, 'comment' => '语音链接'])
		->addColumn('music_title','string',['limit' => 100, 'default' => '', 'null' => true, 'comment' => '音乐标题'])
		->addColumn('music_url','string',['limit' => 255, 'default' => '', 'null' => true, 'comment' => '音乐链接'])
		->addColumn('music_image','string',['limit' => 255, 'default' => '', 'null' => true, 'comment' => '缩略图片'])
		->addColumn('music_desc','string',['limit' => 255, 'default' => '', 'null' => true, 'comment' => '音乐描述'])
		->addColumn('video_title','string',['limit' => 100, 'default' => '', 'null' => true, 'comment' => '视频标题'])
		->addColumn('video_url','string',['limit' => 255, 'default' => '', 'null' => true, 'comment' => '视频URL'])
		->addColumn('video_desc','string',['limit' => 255, 'default' => '', 'null' => true, 'comment' => '视频描述'])
		->addColumn('news_id','biginteger',['default' => '0', 'null' => true, 'comment' => '图文ID'])
		->addColumn('sort','biginteger',['default' => '0', 'null' => true, 'comment' => '排序字段'])
		->addColumn('status','integer',['default' => '1', 'null' => true, 'comment' => '状态(0禁用,1启用)'])
		->addColumn('created_by','biginteger',['default' => '0', 'null' => true, 'comment' => '创建人'])
		->addColumn('create_time','datetime',['default' => 'CURRENT_TIMESTAMP', 'null' => true, 'comment' => '创建时间'])
		->addIndex('type', ['name' => 'idx_sys_wechat_keys_type'])
		->addIndex('keys', ['name' => 'idx_sys_wechat_keys_keys'])
		->addIndex('sort', ['name' => 'idx_sys_wechat_keys_sort'])
		->addIndex('appid', ['name' => 'idx_sys_wechat_keys_appid'])
		->addIndex('status', ['name' => 'idx_sys_wechat_keys_status'])
		->create();

		// 修改主键长度
		$this->table($table)->changeColumn('id','integer',['limit'=>11,'identity'=>true]);
	}

    /**
     * 创建数据对象
     * @class SysWechatMedia
     * @table sys_wechat_media
     * @return void
     */
    private function _create_sys_wechat_media() {

        // 当前数据表
        $table = 'sys_wechat_media';

        // 存在则跳过
        if ($this->hasTable($table)) return;

        // 创建数据表
        $this->table($table, [
            'engine' => 'InnoDB', 'collation' => 'utf8mb4_general_ci', 'comment' => '微信-素材',
        ])
		->addColumn('md5','string',['limit' => 32, 'default' => '', 'null' => true, 'comment' => '文件哈希'])
		->addColumn('type','string',['limit' => 20, 'default' => '', 'null' => true, 'comment' => '媒体类型'])
		->addColumn('appid','string',['limit' => 100, 'default' => '', 'null' => true, 'comment' => '公众号ID'])
		->addColumn('media_id','string',['limit' => 100, 'default' => '', 'null' => true, 'comment' => '永久素材MediaID'])
		->addColumn('local_url','string',['limit' => 300, 'default' => '', 'null' => true, 'comment' => '本地文件链接'])
		->addColumn('media_url','string',['limit' => 300, 'default' => '', 'null' => true, 'comment' => '远程图片链接'])
		->addColumn('create_time','datetime',['default' => 'CURRENT_TIMESTAMP', 'null' => true, 'comment' => '创建时间'])
		->addIndex('md5', ['name' => 'idx_sys_wechat_media_md5'])
		->addIndex('type', ['name' => 'idx_sys_wechat_media_type'])
		->addIndex('appid', ['name' => 'idx_sys_wechat_media_appid'])
		->addIndex('media_id', ['name' => 'idx_sys_wechat_media_media_id'])
		->create();

		// 修改主键长度
		$this->table($table)->changeColumn('id','integer',['limit'=>11,'identity'=>true]);
	}

    /**
     * 创建数据对象
     * @class SysWechatNews
     * @table sys_wechat_news
     * @return void
     */
    private function _create_sys_wechat_news() {

        // 当前数据表
        $table = 'sys_wechat_news';

        // 存在则跳过
        if ($this->hasTable($table)) return;

        // 创建数据表
        $this->table($table, [
            'engine' => 'InnoDB', 'collation' => 'utf8mb4_general_ci', 'comment' => '微信-图文',
        ])
		->addColumn('media_id','string',['limit' => 100, 'default' => '', 'null' => true, 'comment' => '永久素材MediaID'])
		->addColumn('local_url','string',['limit' => 300, 'default' => '', 'null' => true, 'comment' => '永久素材外网URL'])
		->addColumn('article_id','string',['limit' => 60, 'default' => '', 'null' => true, 'comment' => '关联图文ID(用英文逗号做分割)'])
		->addColumn('is_deleted','integer',['default' => '0', 'null' => true, 'comment' => '删除状态(0未删除,1已删除)'])
		->addColumn('create_time','timestamp',['default' => 'CURRENT_TIMESTAMP', 'null' => true, 'comment' => '创建时间'])
		->addColumn('created_by','biginteger',['default' => '0', 'null' => true, 'comment' => '创建人'])
		->addIndex('media_id', ['name' => 'idx_sys_wechat_news_media_id'])
		->addIndex('article_id', ['name' => 'idx_sys_wechat_news_article_id'])
		->create();

		// 修改主键长度
		$this->table($table)->changeColumn('id','integer',['limit'=>11,'identity'=>true]);
	}

    /**
     * 创建数据对象
     * @class SysWechatNewsArticle
     * @table sys_wechat_news_article
     * @return void
     */
    private function _create_sys_wechat_news_article() {

        // 当前数据表
        $table = 'sys_wechat_news_article';

        // 存在则跳过
        if ($this->hasTable($table)) return;

        // 创建数据表
        $this->table($table, [
            'engine' => 'InnoDB', 'collation' => 'utf8mb4_general_ci', 'comment' => '微信-文章',
        ])
		->addColumn('title','string',['limit' => 50, 'default' => '', 'null' => true, 'comment' => '素材标题'])
		->addColumn('local_url','string',['limit' => 300, 'default' => '', 'null' => true, 'comment' => '永久素材URL'])
		->addColumn('show_cover_pic','integer',['default' => '0', 'null' => true, 'comment' => '显示封面(0不显示,1显示)'])
		->addColumn('author','string',['limit' => 20, 'default' => '', 'null' => true, 'comment' => '文章作者'])
		->addColumn('digest','string',['limit' => 300, 'default' => '', 'null' => true, 'comment' => '摘要内容'])
		->addColumn('content','text',['default' => NULL, 'null' => true, 'comment' => '图文内容'])
		->addColumn('content_source_url','string',['limit' => 200, 'default' => '', 'null' => true, 'comment' => '原文地址'])
		->addColumn('read_num','biginteger',['default' => '0', 'null' => true, 'comment' => '阅读数量'])
		->addColumn('create_time','datetime',['default' => 'CURRENT_TIMESTAMP', 'null' => true, 'comment' => '创建时间'])
		->create();

		// 修改主键长度
		$this->table($table)->changeColumn('id','integer',['limit'=>11,'identity'=>true]);
	}

    /**
     * 创建数据对象
     * @class SysWechatPaymentRecord
     * @table sys_wechat_payment_record
     * @return void
     */
    private function _create_sys_wechat_payment_record() {

        // 当前数据表
        $table = 'sys_wechat_payment_record';

        // 存在则跳过
        if ($this->hasTable($table)) return;

        // 创建数据表
        $this->table($table, [
            'engine' => 'InnoDB', 'collation' => 'utf8mb4_general_ci', 'comment' => '微信-支付-行为',
        ])
		->addColumn('type','string',['limit' => 20, 'default' => '', 'null' => true, 'comment' => '交易方式'])
		->addColumn('code','string',['limit' => 20, 'default' => '', 'null' => true, 'comment' => '发起支付号'])
		->addColumn('appid','string',['limit' => 50, 'default' => '', 'null' => true, 'comment' => '发起APPID'])
		->addColumn('openid','string',['limit' => 50, 'default' => '', 'null' => true, 'comment' => '用户OPENID'])
		->addColumn('order_code','string',['limit' => 20, 'default' => '', 'null' => true, 'comment' => '原订单编号'])
		->addColumn('order_name','string',['limit' => 255, 'default' => '', 'null' => true, 'comment' => '原订单标题'])
		->addColumn('order_amount','decimal',['precision' => 20, 'scale' => 2, 'default' => '0.00', 'null' => true, 'comment' => '原订单金额'])
		->addColumn('payment_time','datetime',['default' => NULL, 'null' => true, 'comment' => '支付完成时间'])
		->addColumn('payment_trade','string',['limit' => 100, 'default' => '', 'null' => true, 'comment' => '平台交易编号'])
		->addColumn('payment_status','integer',['default' => '0', 'null' => true, 'comment' => '支付状态(0未付,1已付,2取消)'])
		->addColumn('payment_amount','decimal',['precision' => 20, 'scale' => 2, 'default' => '0.00', 'null' => true, 'comment' => '实际到账金额'])
		->addColumn('payment_bank','string',['limit' => 50, 'default' => '', 'null' => true, 'comment' => '支付银行类型'])
		->addColumn('payment_notify','text',['default' => NULL, 'null' => true, 'comment' => '支付结果通知'])
		->addColumn('payment_remark','string',['limit' => 999, 'default' => '', 'null' => true, 'comment' => '支付状态备注'])
		->addColumn('refund_status','integer',['default' => '0', 'null' => true, 'comment' => '退款状态(0未退,1已退)'])
		->addColumn('refund_amount','decimal',['precision' => 20, 'scale' => 2, 'default' => '0.00', 'null' => true, 'comment' => '退款金额'])
		->addColumn('create_time','datetime',['default' => NULL, 'null' => true, 'comment' => '创建时间'])
		->addColumn('update_time','datetime',['default' => NULL, 'null' => true, 'comment' => '更新时间'])
		->addIndex('type', ['name' => 'idx_sys_wechat_payment_record_type'])
		->addIndex('code', ['name' => 'idx_sys_wechat_payment_record_code'])
		->addIndex('appid', ['name' => 'idx_sys_wechat_payment_record_appid'])
		->addIndex('openid', ['name' => 'idx_sys_wechat_payment_record_openid'])
		->addIndex('order_code', ['name' => 'idx_sys_wechat_payment_record_order_code'])
		->addIndex('create_time', ['name' => 'idx_sys_wechat_payment_record_create_time'])
		->addIndex('payment_trade', ['name' => 'idx_sys_wechat_payment_record_payment_trade'])
		->addIndex('payment_status', ['name' => 'idx_sys_wechat_payment_record_payment_status'])
		->create();

		// 修改主键长度
		$this->table($table)->changeColumn('id','integer',['limit'=>11,'identity'=>true]);
	}

    /**
     * 创建数据对象
     * @class SysWechatPaymentRefund
     * @table sys_wechat_payment_refund
     * @return void
     */
    private function _create_sys_wechat_payment_refund() {

        // 当前数据表
        $table = 'sys_wechat_payment_refund';

        // 存在则跳过
        if ($this->hasTable($table)) return;

        // 创建数据表
        $this->table($table, [
            'engine' => 'InnoDB', 'collation' => 'utf8mb4_general_ci', 'comment' => '微信-支付-退款',
        ])
		->addColumn('code','string',['limit' => 20, 'default' => '', 'null' => true, 'comment' => '发起支付号'])
		->addColumn('record_code','string',['limit' => 20, 'default' => '', 'null' => true, 'comment' => '子支付编号'])
		->addColumn('refund_time','datetime',['default' => NULL, 'null' => true, 'comment' => '支付完成时间'])
		->addColumn('refund_trade','string',['limit' => 100, 'default' => '', 'null' => true, 'comment' => '平台交易编号'])
		->addColumn('refund_status','integer',['default' => '0', 'null' => true, 'comment' => '支付状态(0未付,1已付,2取消)'])
		->addColumn('refund_amount','decimal',['precision' => 20, 'scale' => 2, 'default' => '0.00', 'null' => true, 'comment' => '实际到账金额'])
		->addColumn('refund_account','string',['limit' => 180, 'default' => '', 'null' => true, 'comment' => '退款目标账号'])
		->addColumn('refund_scode','string',['limit' => 50, 'default' => '', 'null' => true, 'comment' => '退款状态码'])
		->addColumn('refund_remark','string',['limit' => 999, 'default' => '', 'null' => true, 'comment' => '支付状态备注'])
		->addColumn('refund_notify','text',['default' => NULL, 'null' => true, 'comment' => '退款交易通知'])
		->addColumn('create_time','datetime',['default' => NULL, 'null' => true, 'comment' => '创建时间'])
		->addColumn('update_time','datetime',['default' => NULL, 'null' => true, 'comment' => '更新时间'])
		->addIndex('code', ['name' => 'idx_sys_wechat_payment_refund_code'])
		->addIndex('create_time', ['name' => 'idx_sys_wechat_payment_refund_create_time'])
		->addIndex('record_code', ['name' => 'idx_sys_wechat_payment_refund_record_code'])
		->addIndex('refund_trade', ['name' => 'idx_sys_wechat_payment_refund_refund_trade'])
		->addIndex('refund_status', ['name' => 'idx_sys_wechat_payment_refund_refund_status'])
		->create();

		// 修改主键长度
		$this->table($table)->changeColumn('id','integer',['limit'=>11,'identity'=>true]);
	}

    /**
     * 创建数据对象
     * @class SysWorldCode
     * @table sys_world_code
     * @return void
     */
    private function _create_sys_world_code() {

        // 当前数据表
        $table = 'sys_world_code';

        // 存在则跳过
        if ($this->hasTable($table)) return;

        // 创建数据表
        $this->table($table, [
            'engine' => 'InnoDB', 'collation' => 'utf8mb4_general_ci', 'comment' => '系统-原产国代码表',
        ])
		->addColumn('world_code_id','string',['limit' => 36, 'default' => NULL, 'null' => true, 'comment' => '标识'])
		->addColumn('cn_abbreviation','string',['limit' => 255, 'default' => NULL, 'null' => true, 'comment' => '中文简称'])
		->addColumn('en_abbreviation','string',['limit' => 255, 'default' => '', 'null' => true, 'comment' => '英文简称'])
		->addColumn('two_char_code','string',['limit' => 4, 'default' => '', 'null' => true, 'comment' => '二字符代码'])
		->addColumn('three_char_code','string',['limit' => 4, 'default' => NULL, 'null' => true, 'comment' => '三字符代码'])
		->addColumn('digit_code','string',['limit' => 4, 'default' => '', 'null' => true, 'comment' => '数字代码'])
		->addColumn('full_name','string',['limit' => 255, 'default' => NULL, 'null' => true, 'comment' => '全称'])
		->addColumn('status','char',['limit' => 1, 'default' => '0', 'null' => true, 'comment' => '状态'])
		->addColumn('sort','biginteger',['default' => '0', 'null' => true, 'comment' => '排序权重'])
		->addColumn('is_deleted','integer',['default' => '0', 'null' => false, 'comment' => '删除状态'])
		->addColumn('create_time','datetime',['default' => 'CURRENT_TIMESTAMP', 'null' => false, 'comment' => '创建时间'])
		->addColumn('created_by','biginteger',['default' => '0', 'null' => false, 'comment' => '创者人'])
		->addColumn('update_time','datetime',['default' => 'CURRENT_TIMESTAMP', 'null' => false, 'comment' => '更新时间'])
		->addColumn('updated_by','biginteger',['default' => '0', 'null' => false, 'comment' => '更新人'])
		->addIndex('is_deleted', ['name' => 'idx_sys_world_code_is_deleted'])
		->addIndex('status', ['name' => 'idx_sys_world_code_status'])
		->addIndex('two_char_code', ['name' => 'idx_sys_world_code_two_char_code'])
		->addIndex('digit_code', ['name' => 'idx_sys_world_code_digit_code'])
		->create();

		// 修改主键长度
		$this->table($table)->changeColumn('id','integer',['limit'=>11,'identity'=>true]);
	}

}
